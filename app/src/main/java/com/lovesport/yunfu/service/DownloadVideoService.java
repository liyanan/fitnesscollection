package com.lovesport.yunfu.service;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Binder;
import android.os.IBinder;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.FileAsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.lovesport.yunfu.Constant;
import com.lovesport.yunfu.javabean.DataItem;
import com.lovesport.yunfu.utils.DownloadVideoUtils;
import com.lovesport.yunfu.utils.OmniStorage;
import org.apache.http.Header;
import java.io.File;
import java.util.HashMap;

public class DownloadVideoService extends Service implements Constant {

    public static final int VIDEO_DOWNED = 0;
    public static final int VIDEO_DOWNING = 1;
    public static final int VIDEO_UNLOAD = 2;
    public static final int VIDEO_DOWNLOAD_FAILURE = 3 ;

    private final IBinder mBinder = new LocalBinder();

    private static AsyncHttpClient mClient = new AsyncHttpClient();

    private DownloadVideoUtils.OnDownLoadListener mOnDownLoadListener;

    private SharedPreferences mSharedPreferences;
    private int mNumBoundClients = 0;

    private HashMap<String,String> videoFlags = new HashMap<>();
    /**
     * Class used for the client Binder.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with IPC.
     */
    public class LocalBinder extends Binder {
        public DownloadVideoService getService () {
            return DownloadVideoService.this;
        }
    }

    public void setOnDownLoadListener (DownloadVideoUtils.OnDownLoadListener onDownLoadListener) {
        this.mOnDownLoadListener = onDownLoadListener;
    }

    @Override
    public IBinder onBind (Intent intent) {
        mNumBoundClients++;
        return mBinder;
    }

    @Override
    public boolean onUnbind (Intent intent) {
        mNumBoundClients--;
        return super.onUnbind(intent);
    }

    /**
     *
     * @param url
     * @param responseHandler
     */
    public  void get (String url, AsyncHttpResponseHandler responseHandler) {
        mClient.setTimeout(20000);
        mClient.setEnableRedirects(true , true);
        mClient.get(url, responseHandler);
    }

    /**
     *
     * @param url
     * @param responseHandler
     */
    public  void get (String url, FileAsyncHttpResponseHandler responseHandler) {
        mClient.setTimeout(20000);
        mClient.setEnableRedirects(true,true);
        mClient.get(url, responseHandler);
    }

    /**
     * @param url
     * @param params
     * @param responseHandler
     */
    public  void get (String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        mClient.setTimeout(20000);
        mClient.setEnableRedirects(true,true);
        mClient.get(url, params, responseHandler);
    }

    public void startDownloadVideo (final DataItem item){

        final File downloadFile ,desFile;
        final String name = item.videoName;
        String url =  item.downLoadUrl;
        final String vid = item.videoVid;

        videoFlags.put(vid , name);
        String downloadedFileName = Math.abs(vid.hashCode()) + ".ttjs.temp";
        downloadFile = OmniStorage.getPublicStorageFile(VIDEO_FOLDER_NAME, downloadedFileName, DownloadVideoService.this);
        String desFileName =  Math.abs(vid.hashCode()) + ".mp4";
        desFile = OmniStorage.getPublicStorageFile(VIDEO_FOLDER_NAME, desFileName, DownloadVideoService.this);
        mSharedPreferences = getSharedPreferences(SHAREDPREFERENCE, MODE_PRIVATE);

        final long fileSpace = mSharedPreferences.getLong(FILESPACE,0l);
        get(url, new FileAsyncHttpResponseHandler(downloadFile) {

            boolean isDownloading = true;
            boolean isFirst = true;
            int  fileSize = 0;

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
                if (file.exists()){
                    file.delete();
                }
                if (mOnDownLoadListener !=null){
                    mOnDownLoadListener.videoDownloadFailure(item);
                }
                long fileSpace = mSharedPreferences.getLong(FILESPACE ,0l);
                mSharedPreferences.edit().putLong(FILESPACE,(fileSpace + fileSize)).apply();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, File file) {
                file.renameTo(desFile);

                if (mOnDownLoadListener != null){
                    mOnDownLoadListener.videoDownloadSuccess(file ,item);
                }
            }

            @Override
            public void onStart(){

                if (mOnDownLoadListener != null){
                    mOnDownLoadListener.videoDownloadStart(item);
                }
            }

            @Override
            public void onProgress(int bytesWritten, int totalSize) {
                super.onProgress(bytesWritten, totalSize);

                if (totalSize >= fileSpace ){
                    isDownloading = false;
                }
                if (mOnDownLoadListener != null){
                    mOnDownLoadListener.getDownloadProgressInfo(bytesWritten , totalSize , item ,isDownloading);
                }
                if (isFirst){
                    fileSize = totalSize;
                    mSharedPreferences.edit().putLong(FILESPACE,(fileSpace - totalSize)).apply();
                    isFirst = false;
                }
            }

            @Override
            public void onFinish() {
                if (videoFlags.containsKey(vid)) {
                    videoFlags.remove(vid);
                    if (videoFlags.size() == 0 && mNumBoundClients == 0) {
                        stopSelf();
                    }
                }

                if (mOnDownLoadListener != null){
                    mOnDownLoadListener.videoDownloadFinish();
                }
            }
        });
    }


    @Override
    public int onStartCommand (Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onCreate () {
        super.onCreate();
    }

    @Override
    public void onDestroy () {
        super.onDestroy();
    }
}

