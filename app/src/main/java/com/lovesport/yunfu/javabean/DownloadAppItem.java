package com.lovesport.yunfu.javabean;
import android.text.TextUtils;
import com.lovesport.yunfu.service.DownloadAppService;

import java.io.File;
import java.lang.String;

/**
 * Created by lyn on 2015/2/3.
 */
public class DownloadAppItem {

    public File mFile;
    public String mUrl;
    public String mPkg;
    public String mName = "";
    public int mStatus = DownloadAppService.DOWNLOAD_STATE_LOADING;
    public int mProgress;

    public DownloadAppItem() {

    }

    public DownloadAppItem(File file, String url, String pkg, String name) {
        if (TextUtils.isEmpty(name)) {
            name = "";
        }
        this.mFile = file;
        this.mUrl = url;
        this.mPkg = pkg;
        this.mName = name;
    }
}
