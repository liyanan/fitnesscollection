package com.lovesport.yunfu.javabean;

/**
 * Created by lyn on 2014/12/29.
 */
public class DataItem {

    public DataItem(){

    }

    public DataItem(String videoName, int videoStatus, String downLoadUrl, String videoVid, String videoPos){
        this.videoName = videoName;
        this.videoStatus = videoStatus;
        this.downLoadUrl = downLoadUrl;
        this.videoVid = videoVid;
        this.videoPos = videoPos;
    }

    public String videoName;
    public int videoStatus;
    public String downLoadUrl;
    public String videoVid;
    public String videoPos;
    public int videoProgress = 0;

}
