package com.lovesport.yunfu.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.lovesport.yunfu.Constant;
import com.lovesport.yunfu.R;
import com.lovesport.yunfu.adapter.WukongLayoutAdapter;
import com.lovesport.yunfu.bus.BusProvider;
import com.lovesport.yunfu.http.TVHttpClient;
import com.lovesport.yunfu.javabean.DataItem;
import com.lovesport.yunfu.javabean.DownloadAppItem;
import com.lovesport.yunfu.javabean.VideoDetails;
import com.lovesport.yunfu.javabean.Videos;
import com.lovesport.yunfu.service.DownloadAppService;
import com.lovesport.yunfu.service.DownloadVideoService;
import com.lovesport.yunfu.utils.JsonParser;
import com.lovesport.yunfu.utils.Utils;
import com.lovesport.yunfu.view.WukongHorizontalLinearLayout;
import com.squareup.otto.Subscribe;
import com.umeng.analytics.MobclickAgent;
import com.wukongtv.http.WKResponseHandler;
import org.json.JSONObject;
import java.util.ArrayList;

/**
 * Created by lyn on 2014/12/8.
 * 专辑详情页面。
 */
public class AlbumDetailsActivity extends Activity implements Constant {

    private Videos videos = new Videos();
    private String mKey;
    private String mPos;
    private WukongHorizontalLinearLayout mLayout;
    private String mType;
    private ViewGroup mRootLayout;
    ArrayList mArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album_details);
        Intent intent = getIntent();
        mPos = intent.getStringExtra(POS);
        mType = intent.getStringExtra(TYPE);
        String name = intent.getStringExtra(NAME);
        if (TextUtils.isEmpty(mType)){
            mType = INDEX;
        }
        mRootLayout = (ViewGroup) findViewById(R.id.activity);
        mKey = intent.getStringExtra(KEY);
        TextView albumName = (TextView) findViewById(R.id.album_details_name);
        albumName.setText(name);
        MobclickAgent.onEvent(this , "album_show" , name);
        mLayout = (WukongHorizontalLinearLayout) findViewById(R.id.wukongLayout);
        final RelativeLayout wukongProgressBar = (RelativeLayout) findViewById(R.id.wukongProgressBar);

        TVHttpClient.getInstance().requestVideos(AlbumDetailsActivity.this, mType, mPos, new WKResponseHandler() {
            @Override
            public void onSuccess(int status, JSONObject responseBody, boolean fromCache) {
                wukongProgressBar.setVisibility(View.GONE);
                videos = JsonParser.parseVideo(responseBody, AlbumDetailsActivity.this);
                mArrayList = videos.videoDetailses;
                insertData(mArrayList);
                mLayout.setAdapter(new WukongLayoutAdapter(AlbumDetailsActivity.this, mArrayList, videos, mKey, mPos));
                TextView jianjie = (TextView) findViewById(R.id.album_details_jianjie);
                if (!TextUtils.isEmpty(videos.videoJianjie)) {
                    jianjie.setText(videos.videoJianjie);
                } else {
                    jianjie.setVisibility(View.GONE);
                }
                if (mLayout.getChildAt(0) != null) {
                    mLayout.getChildAt(0).requestFocus();
                }
            }

            @Override
            public void onFailure(int status, String reason) {

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        Utils.setAlbumBackground(this, mRootLayout, mType);
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
        if (mLayout != null){
            mLayout.bindData();
        }
        BusProvider.getInstance().register(this);
    }


    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
        BusProvider.getInstance().unregister(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Utils.removeAlbumBackground(mRootLayout);
    }

    @Subscribe
    public void onVideoDownloadStatusChange(DataItem dataItem) {
        if (dataItem == null) {
            return;
        }
        int status = dataItem.videoStatus;
        String pos = dataItem.videoPos;
        switch (status) {
            case DownloadVideoService.VIDEO_DOWNED:
                if (!TextUtils.isEmpty(pos) && pos.equals(mPos)){
                        if (mLayout != null){
                            mLayout.bindData();
                        }
                }
                break;
            default:
                break;
        }
    }

    @Subscribe
    public void onTtjsDownloadStatusChange(DownloadAppItem downloadItem) {
        if (downloadItem == null) {
            return;
        }
        int status = downloadItem.mStatus;
        switch (status) {
            case DownloadAppService.DOWNLOAD_STATE_LOADING:
                //正在下载中
                Log.i("mandy", "正在下载中_ AlbumDetailsActivity");
                break;
            case DownloadAppService.DOWNLOAD_STATE_INSTALL_FAILD:
                //下载失败
                Log.i("mandy" ,"下载失败_ AlbumDetailsActivity");
                break;
            case DownloadAppService.DOWNLOAD_STATE_INSTALL_ING:
                //正在安装
                Log.i("mandy" ,"正在安装_ AlbumDetailsActivity");
                break;
            case DownloadAppService.DOWNLOAD_STATE_INSTALL_SUCCESS:
                //安装成功
                Log.i("mandy" ,"安装成功_ AlbumDetailsActivity");
                break;
            case DownloadAppService.DOWNLOAD_STATE_INSTALL_SYSTEM_UI:
                //不支持自动安装，在安装界面操作
                Log.i("mandy" ,"不支持自动安装，在安装界面操作_ AlbumDetailsActivity");
                break;
            default:
                break;
        }
    }

    private void insertData(ArrayList arrayList){
        VideoDetails details = new VideoDetails();
        details.videoTitle = "更多视频";
        details.videoPicName = "";
        details.videoPlayedCount = "0";
        details.videoYouKuId ="";
        arrayList.add (0 , details);
    }
}
