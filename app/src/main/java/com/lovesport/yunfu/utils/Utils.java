package com.lovesport.yunfu.utils;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;


import com.lovesport.yunfu.Constant;
import com.lovesport.yunfu.R;

import java.io.InputStream;

/**
 * Created by lyn on 2014/12/17.
 */
public class Utils implements Constant {

    public static String getCurrentWeek(int position) {
        String weekDay = null;
        switch (position) {
            case 1:
                weekDay = "周日";
                break;
            case 2:
                weekDay = "周一";
                break;
            case 3:
                weekDay = "周二";
                break;
            case 4:
                weekDay = "周三";
                break;
            case 5:
                weekDay = "周四";
                break;
            case 6:
                weekDay = "周五";
                break;
            case 7:
                weekDay = "周六";
                break;
            default:
                break;
        }
        return weekDay;
    }

    public static void removeBackground(View v) {
        if (v == null) {
            return;
        }
        Bitmap bitmap = ((BitmapDrawable)v.getBackground()).getBitmap();
        v.setBackgroundDrawable(null);
        if (bitmap != null && !bitmap.isRecycled()){
            bitmap.recycle();
            bitmap = null;
        }
    }

    public static void setAlbumBackground(Context context ,ViewGroup viewGroup, String type) {
        if (viewGroup == null) {
            return;
        }
        switch (type) {
            case INDEX:
                Bitmap bitmap = readBitmap(context , R.drawable.default_zj);
                viewGroup.setBackgroundDrawable(new BitmapDrawable(bitmap));
                break;
            case JIAN_FEI:
                Bitmap jianfei  = readBitmap(context , R.drawable.jianfei);
                viewGroup.setBackgroundDrawable(new BitmapDrawable(jianfei));
                break;
            case YU_JIA:
                Bitmap yujia = readBitmap(context , R.drawable.yujia);
                viewGroup.setBackgroundDrawable(new BitmapDrawable(yujia));
                break;
            case YANG_SHENG:
                Bitmap yangsheng = readBitmap(context , R.drawable.yangsheng);
                viewGroup.setBackgroundDrawable(new BitmapDrawable(yangsheng));
                break;
            case ER_TONG:
                Bitmap ertong = readBitmap(context , R.drawable.ertong);
                viewGroup.setBackgroundDrawable(new BitmapDrawable(ertong));
                break;
            case JIAN_MEI:
                Bitmap jianmei = readBitmap(context , R.drawable.jianmei);
                viewGroup.setBackgroundDrawable(new BitmapDrawable(jianmei));
                break;
            case SU_XING:
                Bitmap suxing = readBitmap(context , R.drawable.suxing);
                viewGroup.setBackgroundDrawable(new BitmapDrawable(suxing));
                break;
            case QI_CAI:
                Bitmap qicai = readBitmap(context , R.drawable.qicai);
                viewGroup.setBackgroundDrawable(new BitmapDrawable(qicai));
                break;
            default:
                Bitmap default_zj  = readBitmap(context , R.drawable.default_zj);
                viewGroup.setBackgroundDrawable(new BitmapDrawable(default_zj));
                break;
        }
    }

    public static void removeAlbumBackground(ViewGroup viewGroup) {
        if (viewGroup == null){
            return;
        }

        if (viewGroup.getBackground() instanceof BitmapDrawable){
            Bitmap bitmap = ((BitmapDrawable)viewGroup.getBackground()).getBitmap();
            viewGroup.setBackgroundDrawable(null);
            if (bitmap != null && !bitmap.isRecycled()){
                bitmap.recycle();
                bitmap = null;
            }
        }
    }

    public static void removeImage(ImageView imageView){
        if (imageView == null){
            return;
        }
        if (imageView.getDrawable() instanceof BitmapDrawable){
            Bitmap bitmap = ((BitmapDrawable)imageView.getDrawable()).getBitmap();
            imageView.setImageDrawable(null);
            if (bitmap != null && !bitmap.isRecycled()){
                bitmap.recycle();
                bitmap = null;
            }
        }
    }

    public static void setImageDrawable(ImageView imageView , Context context , int resId){
        if (imageView == null){
            return;
        }
        imageView.setImageDrawable(new BitmapDrawable(readBitmap(context , resId)));
    }

    public static Bitmap readBitmap(Context context , int resId) {
        BitmapFactory.Options opt = new BitmapFactory.Options();
        opt.inPreferredConfig = Bitmap.Config.ARGB_8888;
        opt.inPurgeable = true;
        opt.inInputShareable = true;
        //获取资源图片
        InputStream is = context.getResources().openRawResource(resId);
        return BitmapFactory.decodeStream(is, null, opt);
    }

    public static void removeFrameAnimation(View view){
        if (view == null){
            return;
        }
        if (view.getBackground() instanceof AnimationDrawable){
            AnimationDrawable drawable = (AnimationDrawable) view.getBackground();
            for (int i = 0; i< drawable.getNumberOfFrames() ; i++){
                Bitmap bitmap = ((BitmapDrawable)drawable.getFrame(i)).getBitmap();
                view.setBackgroundDrawable(null);
                drawable.setCallback(null);
                if (bitmap != null && !bitmap.isRecycled()){
                    bitmap.recycle();
                    bitmap = null;
                }
            }
        }
    }

    public static AnimationDrawable loadFrameAnimation(Context context){

        AnimationDrawable animationDrawable = new AnimationDrawable();
        animationDrawable.addFrame(new BitmapDrawable(readBitmap(context , R.drawable.buffer_animation1)) , 50);
        animationDrawable.addFrame(new BitmapDrawable(readBitmap(context , R.drawable.buffer_animation2)) , 50);
        animationDrawable.addFrame(new BitmapDrawable(readBitmap(context , R.drawable.buffer_animation3)) , 50);
        animationDrawable.addFrame(new BitmapDrawable(readBitmap(context , R.drawable.buffer_animation4)) , 50);
        animationDrawable.addFrame(new BitmapDrawable(readBitmap(context , R.drawable.buffer_animation5)) , 50);
        animationDrawable.addFrame(new BitmapDrawable(readBitmap(context , R.drawable.buffer_animation6)) , 50);
        animationDrawable.addFrame(new BitmapDrawable(readBitmap(context , R.drawable.buffer_animation7)) , 50);
        animationDrawable.setOneShot(false);
        return animationDrawable;

    }

}
