package com.lovesport.yunfu.utils;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.text.TextUtils;

import com.umeng.analytics.MobclickAgent;

/**
 * Created by lyn on 2014/12/12.
 */
public class UmengUtils {

    /**
     * 获取友盟在线参数 。String类型
     * @param context
     * @param key
     * @param defaultValue
     * @return
     */
    public static String getUmengOnlineParam(Context context, String key, String defaultValue) {
        String result = MobclickAgent.getConfigParams(context, key);
        if (TextUtils.isEmpty(result)) {
            result = defaultValue;
        }
        return result;
    }

    /**
     * 获取友盟在线参数，int类型
     * @param context
     * @param key
     * @param numFormatExceptionValue
     * @return
     */
    public static int getUmengOnlineParam(Context context, String key, int numFormatExceptionValue) {
        int result;
        String str = MobclickAgent.getConfigParams(context, key);
        if (TextUtils.isEmpty(str)) {
            result = numFormatExceptionValue;
        } else {
            try {
                result = Integer.valueOf(str);
            } catch (NumberFormatException e) {
                result = numFormatExceptionValue;
            }
        }
        return result;
    }

    /**
     * 获取到友盟的在线参数.在线参数中配置的为可以升级的渠道。
     * 返回值为true，说明该渠道可以升级。
     * @param umeng
     * @param context
     * @return
     */
    public static boolean getUpdateChannel(String umeng ,Context context){

        boolean isExit = false;
        String updateChannel = MobclickAgent.getConfigParams(context, "UPDATE_CHANNEL");
        if (!TextUtils.isEmpty(updateChannel)){
            String[] umengArray = updateChannel.split(System.getProperty("line.separator"));
            for (String anUmengArray : umengArray) {
                if (umeng.toLowerCase().trim().equals(anUmengArray)) {
                    isExit = true;
                }
            }
        }
        return isExit;
    }

    /**
     * 获取到当前的渠道
     * @param context
     * @return
     */
    public static String getUmengValue(Context context){

        try {
            ApplicationInfo appInfo = context.getPackageManager()
                    .getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA);
            return appInfo.metaData.getString("UMENG_CHANNEL");
        } catch (PackageManager.NameNotFoundException | NullPointerException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取到当前的版本名
     * @param context
     * @return
     */
    public static String  getVersion(Context context){
        String   versionName  = null;
        PackageInfo applicationInfo;
        try {
            applicationInfo= context.getPackageManager()
                    .getPackageInfo(context.getPackageName(),PackageManager.GET_ACTIVITIES);
            versionName = applicationInfo.versionName;
        }catch (Exception ignored){
        }
        return versionName;
    }
}
