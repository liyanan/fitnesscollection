package com.lovesport.yunfu.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;

import com.lovesport.lc.AutoRelativeLayout;


/**
 * Created by lyn on 2014/12/17.
 */
public class WukongLayout extends AutoRelativeLayout {


    private onKeyEventListener mOnKeyListener;


    public WukongLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public WukongLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public WukongLayout(Context context) {
        super(context);
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if(mOnKeyListener !=null) {
            if (event.getAction() == KeyEvent.ACTION_UP) {
                mOnKeyListener.onKeyEvent(event);
            }
        }
        return super.dispatchKeyEvent(event);
    }

    public void setOnKeyDownListen(onKeyEventListener listen){
        this.mOnKeyListener =listen;
    }

    public static interface  onKeyEventListener{
        void onKeyEvent(KeyEvent event);
    }
}
