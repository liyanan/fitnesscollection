package com.lovesport.yunfu.view;

import android.animation.Animator;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import com.lovesport.lc.LC;
import com.lovesport.yunfu.R;
import com.lovesport.yunfu.adapter.WKPageAdapter;

/**
 * Created by lyn on 14-12-18
 */
public  class WukongHorizontalLinearLayout extends ViewGroup{

    private static final int CHILDREN_MAX_NUM = 16;
    private static final int ENLARGE_DURATION = 200;
    private static final int NORMALIZE_DURATION = 100;

    private static final float SCALE_COEFFICIENT = 1.16f;
    private boolean mInAnimation = false;
    private int mFocusChildIndex = INVALID_CHILD_INDEX;
    private int mLastFocusChildIndex = INVALID_CHILD_INDEX;

    WKPageAdapter mAdapter;

    private Drawable mFocusShadow;
    private boolean mFocusedOnGrandChild = false;
    private static final int INVALID_CHILD_INDEX = -1;

    private Rect mChildRect = new Rect();
    private Rect mFocusShadowPaddingRect = new Rect();
    private Rect mChildInvalidateRect = new Rect();
    private int mHalfGapLength;
    private static final int CHILD_WIDTH = 256;
//    private static final int PADDINGRIGHT  = 126;


    public WukongHorizontalLinearLayout(Context context) {
        this(context, null);
    }

    public WukongHorizontalLinearLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
        TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.BasePageLayout);
        float gapLength = LC.getScaledLength(array.getDimension(R.styleable.BasePageLayout_gapLength, 0));
        mHalfGapLength = (int) (gapLength /2);
        LC.resizeView(this);
        setClipChildren(false);
        setClipToPadding(false);
        initMetroLayout(context);
    }

    public WukongHorizontalLinearLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.BasePageLayout);
        float gapLength = LC.getScaledLength(array.getDimension(R.styleable.BasePageLayout_gapLength, 0));
        mHalfGapLength = (int) (gapLength /2);
        LC.resizeView(this);
        setClipChildren(false);
        setClipToPadding(false);
        initMetroLayout(context);
    }

    @Override
    public LayoutParams generateLayoutParams(AttributeSet attrs) {
        LayoutParams layoutParams =  super.generateLayoutParams(attrs);
        LC.resizeLayoutParams(layoutParams);
        return layoutParams;
    }

    public void setAdapter(WKPageAdapter adapter) {
        if (adapter == null){
            return;
        }
        mAdapter = adapter;
        notifyDataSetChanged();

    }

    @Override
    public void addView(View child, int index, LayoutParams params) {
        super.addView(child, index, params);
        child.setOnFocusChangeListener(childFocusChangeListener);
    }


    public void setGapLength(int gapLength) {
        mHalfGapLength = gapLength / 2;
    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        if (mAdapter == null ) {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
            return;
        }

        final int childCount = getChildCount();
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);

        final int unitHeight = heightSize;
        final int unitWidth = LC.getScaledLength(CHILD_WIDTH);
        int newWidthMode = MeasureSpec.EXACTLY;
        int newWidthSize = unitWidth * (childCount + 1) ;
        int newWidthSpec = MeasureSpec.makeMeasureSpec(newWidthSize, newWidthMode);
        int paddingHorizontal = unitWidth / 2;
        setPadding(paddingHorizontal, 0, paddingHorizontal, 0);
        super.onMeasure(newWidthSpec, heightMeasureSpec);

        // now arrange childs
        for (int i = 0; i < childCount; i++) {
            View child = getChildAt(i);
            int childHeightSpec = MeasureSpec
                    .makeMeasureSpec( unitHeight - mHalfGapLength * 2,
                            MeasureSpec.EXACTLY);
            int childWidthSpec = MeasureSpec
                    .makeMeasureSpec( unitWidth - mHalfGapLength * 2,
                            MeasureSpec.EXACTLY);
            child.measure(childWidthSpec, childHeightSpec);
        }

        // set scroll offset
        ViewParent parent = getParent();
        if (parent instanceof HorizontalOverScrollView) {
            ((HorizontalOverScrollView) parent).setOverScrollDistance(paddingHorizontal + 1);
        }

    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        layoutChildren(l, t, r, b);
    }

    private void layoutChildren(int left, int top, int right, int bottom) {
        final int childCount = getChildCount();
        final int parentLeft = getPaddingLeft();
        final int parentRight = right - left - getPaddingRight();
        final int parentTop = getPaddingTop();
        final int parentBottom = bottom - top - getPaddingBottom();
        int currentRightMost = parentLeft;
        int currentChildLeft = parentLeft;
        int currentChildTop = parentTop;
        final int gapLength = mHalfGapLength * 2;
        for (int i = 0; i < childCount; i ++) {
            View childView = getChildAt(i);
            final int childWidth = childView.getMeasuredWidth();
            final int childHeight = childView.getMeasuredHeight();
            int childRight = currentChildLeft + childWidth + gapLength;
            int childBottom = currentChildTop + childHeight + gapLength;
            childView.layout(currentChildLeft + mHalfGapLength,
                    currentChildTop + mHalfGapLength,
                    childRight - mHalfGapLength,
                    childBottom - mHalfGapLength);
            if (childRight > currentRightMost) {
                currentRightMost = childRight;
            }
            if (i + 1 < childCount) {
                currentChildTop = parentTop;
                currentChildLeft = currentRightMost + 1;
            }
        }
    }

    private void initMetroLayout(Context context) {
        mFocusShadow = context.getResources().getDrawable(R.drawable.item_focused);
        mFocusShadow.getPadding(mFocusShadowPaddingRect);
        setChildrenDrawingOrderEnabled(true);
    }

    @Override
    protected boolean drawChild(Canvas canvas, View child, long drawingTime) {
        if (child != getFocusedChild() || mFocusedOnGrandChild || mInAnimation) {
            return super.drawChild(canvas, child, drawingTime);
        }
        child.getDrawingRect(mChildRect);
        offsetDescendantRectToMyCoords(child, mChildRect);
        int xScaled = (int)(mChildRect.width() * SCALE_COEFFICIENT);
        int yScaled = (int)(mChildRect.height() * SCALE_COEFFICIENT);
        int translateX = mChildRect.left - mFocusShadowPaddingRect.left -
                (xScaled - mChildRect.width()) / 2 ;
        int translateY = mChildRect.top - mFocusShadowPaddingRect.top -
                (yScaled - mChildRect.height()) / 2;
        int boundX = mFocusShadowPaddingRect.left
                + mFocusShadowPaddingRect.right + xScaled;
        int boundY = mFocusShadowPaddingRect.top
                + mFocusShadowPaddingRect.bottom + yScaled;
        canvas.save();

        canvas.translate(translateX, translateY);
        mFocusShadow.setBounds(0, 0, boundX, boundY);
        mFocusShadow.draw(canvas);
        canvas.restore();
        return super.drawChild(canvas, child, drawingTime);

    }

    @Override
    protected int getChildDrawingOrder(int childCount, int i) {
        return getRealChildDrawingOrder(childCount, i);
    }

    private int getRealChildDrawingOrder(int childCount, int i) {
        if (mFocusChildIndex == INVALID_CHILD_INDEX) {
            return super.getChildDrawingOrder(childCount, i);
        }
        if (mLastFocusChildIndex == INVALID_CHILD_INDEX
                || mLastFocusChildIndex == mFocusChildIndex) {
            // last focus index is null
            if (i == childCount - 1) {
                return mFocusChildIndex;
            } else if ( i >= mFocusChildIndex) {
                return i + 1;
            } else {
                return i;
            }
        } else {
            // both last focus index and current focus index
            // are valid
            final int smallerIndex = Math.min(mFocusChildIndex, mLastFocusChildIndex);
            final int biggerIndex = Math.max(mFocusChildIndex, mLastFocusChildIndex);
            if (i == childCount -1) {
                return mFocusChildIndex;
            } else if (i == childCount - 2) {
                return mLastFocusChildIndex;
            } else if (i >= biggerIndex - 1) {
                return i + 2;
            } else if (i >= smallerIndex) {
                return i + 1;
            } else {
                return i;
            }
        }
    }

    @Override
    public void requestChildFocus(View child, View focused) {
        super.requestChildFocus(child, focused);
        mLastFocusChildIndex = mFocusChildIndex;
        mFocusChildIndex = indexOfChild(child);
        mFocusedOnGrandChild = child != focused;
    }

    public OnFocusChangeListener childFocusChangeListener = new OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            // all children focus handler
            Boolean isEnlarged = (Boolean) v.getTag(R.id.tag_enlarged);
            if (null == isEnlarged) {
                isEnlarged = false;
            }
            final int fullGapLength = mHalfGapLength * 2;
            if (hasFocus) {
                if (!isEnlarged) {
                    // enlarge it
                    enlargeChild(v);
                    // invalidate current focus to draw shadow
                    v.getDrawingRect(mChildInvalidateRect);
                    offsetDescendantRectToMyCoords(v, mChildInvalidateRect);
                    mChildInvalidateRect.top -= mFocusShadowPaddingRect.top + fullGapLength;
                    mChildInvalidateRect.bottom += mFocusShadowPaddingRect.bottom + fullGapLength;
                    mChildInvalidateRect.left -= mFocusShadowPaddingRect.left + fullGapLength;
                    mChildInvalidateRect.right += mFocusShadowPaddingRect.right + fullGapLength;
                    invalidate(mChildInvalidateRect);
                }
                v.setTag(R.id.tag_enlarged, true);
            } else {
                if (isEnlarged) {
                    normalizeChild(v);
//                    // invalidate last focus to erase shadow
                    v.getDrawingRect(mChildInvalidateRect);
                    offsetDescendantRectToMyCoords(v, mChildInvalidateRect);
                    mChildInvalidateRect.top -= mFocusShadowPaddingRect.top + 300;
                    mChildInvalidateRect.bottom += mFocusShadowPaddingRect.bottom + 300;
                    mChildInvalidateRect.left -= mFocusShadowPaddingRect.left + 300;
                    mChildInvalidateRect.right += mFocusShadowPaddingRect.right + 300;
                    invalidate(mChildInvalidateRect);
                }
                v.setTag(R.id.tag_enlarged, false);
            }
        }
    };

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
    private void enlargeChild(final View child) {
        child.animate()
                .scaleX(SCALE_COEFFICIENT)
                .scaleY(SCALE_COEFFICIENT)
                .setDuration(ENLARGE_DURATION)
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        mInAnimation = true;
                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        mInAnimation = false;
                        child.getDrawingRect(mChildInvalidateRect);
                        offsetDescendantRectToMyCoords(child, mChildInvalidateRect);
                        // 这是一个丑陋的hack，不要学我！不要学我！
                        mChildInvalidateRect.top -= mFocusShadowPaddingRect.top + 300;
                        mChildInvalidateRect.bottom += mFocusShadowPaddingRect.bottom + 300;
                        mChildInvalidateRect.left -= mFocusShadowPaddingRect.left + 300;
                        mChildInvalidateRect.right += mFocusShadowPaddingRect.right + 300;
                        invalidate(mChildInvalidateRect);
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {
                        mInAnimation = false;
                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                });
        mInAnimation = true;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
    private void normalizeChild(final View child) {
        child.animate().scaleX(1.0f).scaleY(1.0f).setDuration(NORMALIZE_DURATION)
        .setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
//                child.getDrawingRect(mChildInvalidateRect);
//                offsetDescendantRectToMyCoords(child, mChildInvalidateRect);
//                mChildInvalidateRect.top -= mFocusShadowPaddingRect.top + 100;
//                mChildInvalidateRect.bottom += mFocusShadowPaddingRect.bottom + 100;
//                mChildInvalidateRect.left -= mFocusShadowPaddingRect.left + 100;
//                mChildInvalidateRect.right += mFocusShadowPaddingRect.right + 100;
//                invalidate(mChildInvalidateRect);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }

    public void bindData() {
        if (mAdapter != null) {
            if (mAdapter.getCount() > getChildCount()) {
                notifyDataSetChanged();
            }
            for (int i = 0; i < mAdapter.getCount(); i++) {
                mAdapter.onBindData(i, getChildAt(i));
            }
        }
    }

    public void unbindData() {
        if (mAdapter != null) {
            if (mAdapter.getCount() > getChildCount()) {
                notifyDataSetChanged();
            }
            for (int i = 0; i < mAdapter.getCount(); i++) {
                mAdapter.onUnBindData(i, getChildAt(i));
            }
        }
    }

    public void notifyDataSetChanged() {
        if (mAdapter != null) {
            for (int i = 0; i < mAdapter.getCount(); i++) {
                if (i < CHILDREN_MAX_NUM) {
                    View newView = mAdapter.getView(i, this, getChildAt(i));
                    if (newView.getParent() != this) {
                        addView(newView);
                    }
                }
            }
        }
    }
}
