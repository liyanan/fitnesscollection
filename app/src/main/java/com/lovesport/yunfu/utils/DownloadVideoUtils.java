package com.lovesport.yunfu.utils;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.StatFs;
import android.text.TextUtils;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.lovesport.yunfu.Constant;
import com.lovesport.yunfu.R;
import com.lovesport.yunfu.bus.BusProvider;
import com.lovesport.yunfu.dialog.VideoDownloadDialog;
import com.lovesport.yunfu.http.TVHttpClient;
import com.lovesport.yunfu.javabean.DataItem;
import com.lovesport.yunfu.service.DownloadVideoService;
import com.umeng.analytics.MobclickAgent;

import org.apache.http.Header;
import org.json.JSONObject;

import java.io.File;

/**
 * Created by Administrator on 2014/10/8.
 */
public class DownloadVideoUtils implements Constant {

    private DownloadVideoService mService;
    private boolean mBound;

    private DownloadVideoUtils() {

    }

    private static DownloadVideoUtils mDownloadVideoUtils;

    public static DownloadVideoUtils getInstance() {
        if (mDownloadVideoUtils == null) {
            synchronized (DownloadVideoUtils.class){
                if (mDownloadVideoUtils == null){
                    mDownloadVideoUtils = new DownloadVideoUtils();
                }
            }
        }
        return mDownloadVideoUtils;
    }

    public void startService(Context context) {

        Intent i = new Intent(context, DownloadVideoService.class);
        context.startService(i);
        if (!mBound) {
            try {
                context.bindService(i, mConnection, Context.BIND_AUTO_CREATE);
            } catch (Exception ignored) {

            }
        }
    }

    /**
     * 解除绑定
     */
    public void unbindService(Context context) {
        if (mBound && context != null) {
            try {
                context.unbindService(mConnection);
                mBound = false;
            } catch (Exception ignored) {

            }
        }
    }

    /**
     * 用于下载的监听
     */

    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            DownloadVideoService.LocalBinder binder = (DownloadVideoService.LocalBinder) service;
            mService = binder.getService();
            mBound = true;

            mService.setOnDownLoadListener(new OnDownLoadListener() {

                boolean isDownloading = true;
                boolean isShowing = false;

                @Override
                public void getDownloadProgressInfo(int bytesWritten, int totalSize, DataItem dataItem, boolean isDownloading) {
                    if (!isDownloading && !isShowing) {
                        try {
                            VideoDownloadDialog downloadDialog = new VideoDownloadDialog(mService, true, false, dataItem);
                            downloadDialog.show();
                            this.isDownloading = isDownloading;
                            isShowing = true;
                            dataItem.videoStatus = DownloadVideoService.VIDEO_UNLOAD;
                        } catch (Exception ignored) {

                        }
                    }
                    bytesWritten = bytesWritten / 1024;
                    totalSize = totalSize / 1024;
                    dataItem.videoProgress = bytesWritten * 100/ totalSize ;
                    dataItem.videoStatus = DownloadVideoService.VIDEO_DOWNING;
                    BusProvider.getInstance().post(dataItem);
                }

                @Override
                public void videoDownloadStart(DataItem dataItem) {
                    MobclickAgent.onEvent(mService, "downloadVideo_startDownloadVideo", dataItem.videoName);
                }

                @Override
                public void videoDownloadSuccess(File f, DataItem dataItem) {
                    dataItem.videoStatus = DownloadVideoService.VIDEO_DOWNED;
                    MobclickAgent.onEvent(mService, "downloadVideo_Success", dataItem.videoName);
                    BusProvider.getInstance().post(dataItem);
                }

                @Override
                public void videoDownloadFailure(DataItem dataItem) {
                    if (isDownloading) {
                        Toast.makeText(mService, mService.getResources().getString(R.string.downloadvideofailure), Toast.LENGTH_LONG).show();
                        dataItem.videoStatus = DownloadVideoService.VIDEO_DOWNLOAD_FAILURE;
                    }
                    MobclickAgent.onEvent(mService, "downloadVideo_Failure", dataItem.videoName);
                    BusProvider.getInstance().post(dataItem);
                }

                @Override
                public void videoDownloadFinish() {

                }
            });
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };

    public interface OnDownLoadListener {

        void getDownloadProgressInfo(int bytesWritten, int totalSize, DataItem dataItem, boolean isDownloading);

        void videoDownloadStart(DataItem dataItem);

        void videoDownloadSuccess(File f, DataItem dataItem);

        void videoDownloadFailure(DataItem dataItem);

        void videoDownloadFinish();
    }


    /**
     * 该方法用于判断视频是否已经下载
     * @param name
     * @return
     */
    public  boolean isVideoDownloaded(int name , Context context) {

        File file = OmniStorage.getPublicStorageFile(VIDEO_FOLDER_NAME, name + ".mp4", context);
        return file != null && file.exists() && file.length() != 0;
    }

    /**
     * 该方法用于获得视频的下载地址
     * @param name
     * @return
     */
    public  String getVideoFilePath(int name ,Context context) {

        File file = OmniStorage.getPublicStorageFile(VIDEO_FOLDER_NAME, name + ".mp4", context);
        if (file != null && file.exists()) {
            return file.getAbsolutePath();
        }
        return null;
    }

    /**
     * 该方法用于判断当前路径的剩余空间大小
     *
     * @param file
     */
    public  long getMemorySpace(File file) {

        StatFs statFs = new StatFs(file.getAbsolutePath());

        long blockSize = statFs.getBlockSize();
        long availableBlocks = statFs.getAvailableBlocks();
        return blockSize * availableBlocks;
    }


    /**
     * 该方法用于判断该视频是需要被下载还是可以打开播放。
     *
     * @param item
     */

    public void isDownloadOrOpenVideo(final DataItem item) {

        int flag = -1;
        if (item != null) {
            flag = item.videoStatus;
        }

        switch (flag) {
            case DownloadVideoService.VIDEO_UNLOAD://未下载
                item.videoStatus = DownloadVideoService.VIDEO_DOWNING;
                TVHttpClient.getInstance().requestVideoDownloadUrl(item.downLoadUrl, new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        JSONObject urlObj = JsonParser.getJsonObjectFormBytes(responseBody);
                        if (urlObj != null) {
                            item.downLoadUrl = JsonParser.parseDownloadUrl(urlObj);
                            if (mService != null && !TextUtils.isEmpty(item.downLoadUrl)) {
                                mService.startDownloadVideo(item);
                            }else if (TextUtils.isEmpty(item.downLoadUrl)){
                                downloadFailed(item);
                            }
                        } else{
                            downloadFailed(item);
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        downloadFailed(item);
                    }
                });
                break;
            default:
                break;
        }
    }

    private void downloadFailed(DataItem item) {
        if (mService != null){
            Toast.makeText(mService, mService.getString(R.string.failed_to_get_downloadUrl), Toast.LENGTH_SHORT).show();
        }
        item.videoStatus = DownloadVideoService.VIDEO_DOWNLOAD_FAILURE;
        BusProvider.getInstance().post(item);
    }
}
