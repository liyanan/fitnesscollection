package com.lovesport.yunfu.dialog;

import android.content.Context;
import android.graphics.PixelFormat;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.lovesport.yunfu.R;
import com.lovesport.yunfu.view.WukongLayout;
import com.umeng.analytics.MobclickAgent;


/**
 * Created by lyn on 2014/10/21.
 */
public class VideoDownloadingDialog{

    private Context mContext;
    private View mContentView;
    private WindowManager mWindowManager;
    private boolean isShown;
    private ProgressBar mProgressBar;
    private TextView mProgress;

    public VideoDownloadingDialog(Context context) {
        mContext = context;
        initView();
    }

    private void initView() {
        if (mContext == null) {
            return;
        }
        mContentView = View.inflate(mContext, R.layout.dialog_videoloading, null);
        MobclickAgent.onEvent(mContext , "downloadVideo_downloading_show");
        WukongLayout layout = (WukongLayout) mContentView.findViewById(R.id.layout);
        layout.setOnKeyDownListen(new WukongLayout.onKeyEventListener() {
            @Override
            public void onKeyEvent(KeyEvent event) {
                if (event.getKeyCode() == KeyEvent.KEYCODE_BACK ){
                    if (mContentView != null){
                        dismiss();
                        MobclickAgent.onEvent(mContext , "downloadVideo_downloading_back");
                    }
                }
            }
        });
        mProgressBar = (ProgressBar) mContentView.findViewById(R.id.downloadvideo_progress_bar);
        mProgressBar.setMax(100);
        mProgress = (TextView) mContentView.findViewById(R.id.downloadvideo_progress);
    }

    public void setDownloadProgress(int progress) {
        if (mProgressBar != null && mProgress != null){
            mProgressBar.setProgress(progress);
            mProgress.setText(mContext.getString(R.string.downloadvideo_loading_progress , progress));
        }
    }

    public void show() {
        mWindowManager = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        layoutParams.flags = WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM;
        layoutParams.type = WindowManager.LayoutParams.TYPE_SYSTEM_ALERT;
        layoutParams.format = PixelFormat.TRANSLUCENT;
        layoutParams.x = 0;
        layoutParams.y = 0;
        if (!isShown) {
            try {
                mWindowManager.addView(mContentView, layoutParams);
                isShown = true;
            } catch (Exception e) {
                isShown = false;
            }
        }
    }

    public void dismiss() {
        if (mContentView != null && isShown) {
            try {
                mWindowManager.removeView(mContentView);
                isShown = false;
            } catch (Exception e) {

            }
        }
    }
}
