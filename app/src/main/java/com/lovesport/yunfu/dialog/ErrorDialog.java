package com.lovesport.yunfu.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.lovesport.yunfu.R;
import com.umeng.analytics.MobclickAgent;

public class ErrorDialog extends Dialog {
    private CallBack mCallBack;
	Context context;

    public ErrorDialog(Context context, int theme) {
        super(context, theme);
        this.context = context;
    }

	public ErrorDialog(Context context) {
		super(context);
		this.context = context;
	}

    public void setCallBack(CallBack callBack){
        this.mCallBack = callBack;
    }

	@Override
	public void onBackPressed() {
        super.onBackPressed();
        if (mCallBack != null){
            mCallBack.setActivityCallBack();
        }
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_error);
        Button close;
        close = (Button) findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                MobclickAgent.onEvent(context , "error_dialog_quit");
                if (mCallBack != null){
                    mCallBack.setActivityCallBack();
                }
            }
        });
        MobclickAgent.onEvent(context , "error_dialog_show");
	}

    public interface CallBack{
        public void setActivityCallBack();
    }

}