package com.lovesport.yunfu.utils;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class OmniStorage {

    private static final String TAG = "OmniStorage";
    private Context mContext;

    boolean mExternalStorageAvailable = false;
    boolean mExternalStorageWriteable = false;


    public OmniStorage(Context context) {
        mContext = context;
    }

    public void init() {
        checkExternalStorage();
    }

    /*
     * I want a place to save my file and I need other apps can access it
     * to get FileOutputStream, please use StorageManager.get...Stream
     * @param desiredDirName: relative dir name
     */
    public File getPublicStorage(String desiredDirName) {
        File publicStorage;
        if (mExternalStorageWriteable) {
            publicStorage = getExternalStorage(desiredDirName);
        } else {
            publicStorage = mContext.getFilesDir();
        }

        return publicStorage;
    }

    /*
     * I want a place to save my file and I don't give a shit about access
     * to get FileOutputStream, please use StorageManager.get...Stream
     * @param desiredDirName: relative dir name
     */
    public File getWhateverStorage(String desiredDirName) {
        File weStorage;
        if (mExternalStorageWriteable) {
            weStorage = getExternalStorage(desiredDirName);
        } else {
            weStorage = mContext.getDir(desiredDirName, 0);
        }
        return weStorage;
    }

    public File getCacheDir() {
        if (mExternalStorageWriteable) {
            return mContext.getExternalCacheDir();
        } else {
            return mContext.getCacheDir();
        }
    }

    public FileOutputStream getPublicFileOutputStream(File dir, String filename)
            throws FileNotFoundException {
        if (mExternalStorageWriteable) {
            return new FileOutputStream(new File(dir, filename));
        }
        try {
            if (dir.getCanonicalPath().equals(mContext.getFilesDir().getCanonicalPath())) {
                return mContext.openFileOutput(filename, Context.MODE_WORLD_READABLE);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new FileOutputStream(new File(dir, filename));
    }

    public FileOutputStream getWhateverFileOutputStream(File dir, String filename)
            throws FileNotFoundException {
        return new FileOutputStream(new File(dir, filename));
    }

    private File getExternalStorage(String desiredDirName) {
        /** Returns null if external storage is not currently mounted **/
        File appRoot = mContext.getExternalFilesDir(null);
        File desired_dir = appRoot;
        if (desiredDirName != null) {
            desired_dir = new File(appRoot, desiredDirName);
        }
        if (null != desired_dir && !desired_dir.isDirectory()) {
            desired_dir.mkdirs();
        }
        if (null != desired_dir && desired_dir.isDirectory()) {
            return desired_dir;
        }
        return appRoot;
    }

    private void checkExternalStorage() {

        String state = Environment.getExternalStorageState();

        switch (state) {
            case Environment.MEDIA_MOUNTED:
                // We can read and write the media
                mExternalStorageAvailable = mExternalStorageWriteable = true;
                break;
            case Environment.MEDIA_MOUNTED_READ_ONLY:
                // We can only read the media
                mExternalStorageAvailable = true;
                mExternalStorageWriteable = false;
                break;
            default:
                // Something else is wrong. It may be one of many other states, but all we need
                //  to know is we can neither read nor write
                mExternalStorageAvailable = mExternalStorageWriteable = false;
                break;
        }

        if (mExternalStorageWriteable) {
            File appRoot = mContext.getExternalFilesDir(null);
            if (appRoot == null) {
                mExternalStorageWriteable = mExternalStorageAvailable = false;
            }
        }
        Log.v(TAG, String.format("external storage available: %s, external storage writeable: %s ", mExternalStorageAvailable, mExternalStorageWriteable));
    }

    public static File getPublicStorageFile(String desiredDirName, String fileName, Context context) {
        if(null ==context){
            return null;
        }
        File file, destDir;
        file = Environment.getExternalStoragePublicDirectory("Download");
        if (file != null) {
            destDir = new File(file, desiredDirName);
            if (checkFileOkForStorage(destDir)) {
                return new File(destDir, fileName);
            }
        }

        file = context.getExternalFilesDir(null);
        if (file != null) {
            destDir = new File(file, desiredDirName);
            if (checkFileOkForStorage(destDir)) {
                return new File(destDir, fileName);
            }
        }
        File innerDir = context.getFilesDir();
        try {
            FileOutputStream fos = context.openFileOutput(fileName,
                    Context.MODE_WORLD_WRITEABLE | Context.MODE_WORLD_READABLE);
            fos.close();
        } catch (IOException ignore) {
        }
        return new File(innerDir, fileName);
    }

    public static boolean checkFileOkForStorage(File file) {
        if (file == null) {
            return false;
        } else if (file.exists()) {
            if (!file.isDirectory()) {
                return false;
            }
        } else {
            if (!file.mkdirs()) {
                return false;
            }
        }
        return true;
    }
}
