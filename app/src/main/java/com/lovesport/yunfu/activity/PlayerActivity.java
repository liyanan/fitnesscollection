package com.lovesport.yunfu.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.lovesport.fitCommon.BasePlayerActivity;
import com.lovesport.yunfu.Constant;
import com.lovesport.yunfu.R;
import com.lovesport.yunfu.WKHandler;
import com.lovesport.yunfu.bus.BusProvider;
import com.lovesport.yunfu.dialog.ErrorDialog;
import com.lovesport.yunfu.dialog.PauseDialog;
import com.lovesport.yunfu.dialog.QuitVideoDialog;
import com.lovesport.yunfu.dialog.VideoDownloadDialog;
import com.lovesport.yunfu.dialog.VideoDownloadedDialog;
import com.lovesport.yunfu.dialog.VideoDownloadingDialog;
import com.lovesport.yunfu.http.TVHttpClient;
import com.lovesport.yunfu.javabean.DataItem;
import com.lovesport.yunfu.service.DownloadVideoService;
import com.lovesport.yunfu.utils.DownloadVideoUtils;
import com.lovesport.yunfu.utils.TimeUtil;
import com.lovesport.yunfu.utils.UmengUtils;
import com.lovesport.yunfu.utils.Utils;
import com.lovesport.yunfu.view.WukongProgressBar;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.squareup.otto.Subscribe;
import com.umeng.analytics.MobclickAgent;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by lyn on 2014/12/10.
 */
public class PlayerActivity extends BasePlayerActivity implements Constant {

    public static final int MSG_NOTIFICATION_TIME = 0x127;
    public static final int MSG_DELAY_TIME = 40 * 1000;
    public static final int MSG_SHORT_DELAY_TIME = 2 * 1000;
    public static final int MSG_VIDEO_DOWNLOADED_DIALOG_DISMISS = 0x128;
    public static final int BUFFER_TIME = 20;

    private boolean isResumed;
    private View mBufferingIcon;
    private ImageView mBufferingSpin;
    private boolean mBufferingShowing;
    private RelativeLayout mRlTarget;
    private TextView mTvTarget;
    private boolean hasTarget = false;
    private String mTarget = null ;
    private ImageView mErweima;
    private String mVid;
    private String mPos;
    private String mTitle;
    private SharedPreferences mSharedPreferences;
    private boolean isTodayFirstPlayVideo = false;
    private DisplayImageOptions mOptions;
    private DataItem mDataItem;
    private int mBufferingIndex = 0;
    private boolean isPlaylocalVideo = false;
    private boolean isDialogShowing = false;
    private VideoDownloadDialog mDownloadDialog;
    private Handler mDelayedHandler;
    private DownloadVideoUtils mDownloadVideoUtil;
    private String mDownloadFile;
    private VideoDownloadingDialog mVideoDownloadingDialog;
    private VideoDownloadedDialog mVideoDownloadedDialog;
    private int mVideoStatus;
    private float mKey;
    private WukongProgressBar mWukongProgressBar;
    private TextView mVideoPosition;
    private TextView mCalorie;
    private static final int DELAY = 74;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        String api = intent.getStringExtra(PLAY_URI_API);
        mVid = intent.getStringExtra(VIDEO_ID);
        mTitle  = intent.getStringExtra(PLAY_TITLE);
        String key = intent.getStringExtra(PLAY_KEY);
        mPos = intent.getStringExtra(POS);
        String playUrl = api + "?vid=" + mVid + "&type=bofang&qudao=" +
                UmengUtils.getUmengValue(this) + "&ver="+ UmengUtils.getVersion(this);

        Map maps = new HashMap();
        maps.put(PLAY_URI_API , playUrl); //播放地址
        maps.put(PLAY_TITLE , mTitle);//视频名称P

        try {
            mKey = Float.parseFloat(key);
        }catch (Exception e){
            mKey = 0;
        }

        mVideoStatus = intent.getIntExtra(VIDEO_STATUS
                , DownloadVideoService.VIDEO_UNLOAD);

        if (mVideoStatus == DownloadVideoService.VIDEO_DOWNED){
            mDownloadFile = DownloadVideoUtils.getInstance()
                    .getVideoFilePath(Math.abs(mVid.hashCode()) ,PlayerActivity.this);
            maps.put(PLAY_LOCAL_PATH, mDownloadFile);
            setItemContent(maps);
            isPlaylocalVideo = true;
        } else {
            setItemContent(maps);
        }

        String downloadUrl = api + "?vid=" + mVid + "&type=xiazai&qudao="
                + UmengUtils.getUmengValue(this) + "&ver="+ UmengUtils.getVersion(this);
        mDataItem  = new DataItem(mTitle , mVideoStatus , downloadUrl , mVid , mPos);

        mSharedPreferences = getSharedPreferences(SHAREDPREFERENCE ,  MODE_PRIVATE);
        long playVideoTime = mSharedPreferences.getLong(TODAY_FIRST_PLAYVIDEO_TIME, 0);
        if (DateUtils.isToday(playVideoTime)) {
            isTodayFirstPlayVideo = false;
        } else {
            isTodayFirstPlayVideo = true;
            playVideoTime = System.currentTimeMillis();
            mSharedPreferences.edit().putLong(TODAY_FIRST_PLAYVIDEO_TIME, playVideoTime).apply();
        }

        initView();
        setAnimation();
        MobclickAgent.onEvent(this, "video_play", mTitle);

        mDownloadVideoUtil = DownloadVideoUtils.getInstance();

        mOptions = new DisplayImageOptions.Builder()
                .showImageForEmptyUri(R.drawable.erwei)
                .showImageOnFail(R.drawable.erwei)
                .cacheInMemory(false)
                .cacheOnDisc(true)
                .bitmapConfig(Bitmap.Config.ARGB_8888)
                .build();

        mDelayedHandler = new DelayHandler(this);
        MobclickAgent.onEvent(this , "onCreate" , mVid + "|" + mPos +"|" + mTitle);
    }


    @Override
    public void onVideoStart() {
        //视频开始播放
        mDelayedHandler.post(updateCaloRunnable);
    }

    @Override
    public void onVideoPause() {
        removeRunnable();
        //视频暂停播放
        if (isResumed){
            PauseDialog pauseDialog = new PauseDialog(PlayerActivity.this);
            pauseDialog.setCallBack(new PauseDialog.PauseDialogCallBack() {
                @Override
                public void continuePlay() {
                    continuePlayVideo();
                    dismissMediaController();
                }
            });
            pauseDialog.show();
        }
    }

    @Override
    public void onVideoContinue() {
        postRunnable();
    }

    @Override
    public void onVideoCompletion() {
        //视频播放完成
        MobclickAgent.onEvent(this , "video_play_completion" , mTitle);
        finish();
    }

    @Override
    public void onVideoRewStart() {
        removeRunnable();
    }

    @Override
    public void onVideoRewEnd() {
        postRunnable();
    }

    @Override
    public void onVideoFfwdStart() {
        removeRunnable();
    }

    @Override
    public void onVideoFfwdEnd() {
        postRunnable();
    }

    @Override
    public void onVideoPlayError() {
        MobclickAgent.onEvent(this , "onVideoPlayError" , mVid + "|" + mPos +"|" + mTitle);
        if (isResumed){
            showBufferingAnimation(false);
            ErrorDialog errorDialog = new ErrorDialog(this , R.style.error);
            errorDialog.setCallBack(new ErrorDialog.CallBack() {
                @Override
                public void setActivityCallBack() {
                    finish();
                }
            });
            errorDialog.show();
        }
        MobclickAgent.onEvent(this , "video_play_error1" , mTitle +"|"+ mVid);
        TVHttpClient.getInstance().reportPlayError(this , mVid , mPos);
    }

    @Override
    public void onVideoBufferStart() {
        //视频开始缓冲
        removeRunnable();
        showBufferingAnimation(true);
    }

    @Override
    public void onVideoBufferEnd() {
        postRunnable();
        //视频缓冲结束
        showBufferingAnimation(false);
    }

    @Override
    public void initContent(ViewGroup viewGroup) {
        //可以加入一些播放库以外的ui
        View playView = View.inflate(this, R.layout.activity_player, null);
        mBufferingIcon = playView.findViewById(R.id.buffering);
        mBufferingSpin = (ImageView) playView.findViewById(R.id.buffering_spin);

        mRlTarget = (RelativeLayout) playView.findViewById(R.id.rl_target);
        mTvTarget = (TextView) playView.findViewById(R.id.tv_player_target);
        mErweima = (ImageView) playView.findViewById(R.id.erweima);
        ImageLoader.getInstance().displayImage(getString(R.string.erweima) , mErweima , mOptions );

        //卡路里
        LinearLayout wukongProgressView = (LinearLayout) playView.findViewById(R.id.wukongProgressBarView);
        mWukongProgressBar = (WukongProgressBar) playView.findViewById(R.id.wukongProgressBar);
        mCalorie = (TextView) playView.findViewById(R.id.calorie);
        mVideoPosition = (TextView) playView.findViewById(R.id.video_position);
        if (mKey == 0){
            wukongProgressView.setVisibility(View.GONE);
            mWukongProgressBar.setVisibility(View.GONE);
            mCalorie.setVisibility(View.GONE);
            mVideoPosition.setVisibility(View.GONE);
        }

        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        viewGroup.addView(playView, layoutParams);
    }

    @Override
    public void handler() {
        //每秒会执行一次该方法。
        if (mVideoView != null && !TextUtils.isEmpty(mTarget)) {
            int duration = mVideoView.getDuration();
            int position = mVideoView.getCurrentPosition();
            if (!hasTarget && duration > 0) {
                if (duration - position <= 30000) {
                    disPlayTarget(position);
                    hasTarget = true;
                }
            }
        }

        //设置卡路里进度条的当前播放时间
        if (mVideoView != null) {
            int position = mVideoView.getCurrentPosition();
            if (mVideoPosition != null) {
                mVideoPosition.setText(TimeUtil.stringForTime(position));
            }
            if (mWukongProgressBar != null) {
                int duration = mVideoView.getDuration();
                if (duration != 0){
                    long progress = 360 * position / duration;
                    mWukongProgressBar.setProgress((int) progress);
                }
            }
            if (mVideoView.isPlaying() && !mVideoView.isBuffering()) {
                showBufferingAnimation(false);
            }
        }

        if (mBufferingShowing) {
            mBufferingIndex++;
        }

        if (mBufferingIndex == BUFFER_TIME && mVideoStatus == DownloadVideoService.VIDEO_UNLOAD && !isPlaylocalVideo && !isDialogShowing) {
            isDialogShowing = true;
            mDownloadDialog = new VideoDownloadDialog(PlayerActivity.this, false, false, mDataItem);
            mDownloadDialog.setVideoDownloadDialogCallBack(callBack);
            mDownloadDialog.show();
            if (mDelayedHandler != null){
                mDelayedHandler.sendEmptyMessageDelayed(MSG_NOTIFICATION_TIME, MSG_DELAY_TIME);
            }
        }
    }

    private VideoDownloadDialog.VideoDownloadDialogCallBack callBack = new VideoDownloadDialog.VideoDownloadDialogCallBack() {
        @Override
        public void showDownloadingDialog() {
            mVideoDownloadingDialog = new VideoDownloadingDialog(PlayerActivity.this);
            mVideoDownloadingDialog.show();
        }
    };

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onEvent(this , "onPause" , mVid + "|" + mPos +"|" + mTitle);
        isResumed = false;
        MobclickAgent.onPause(this);
        BusProvider.getInstance().unregister(this);
        mDownloadVideoUtil.unbindService(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onEvent(this , "onResume" , mVid + "|" + mPos +"|" + mTitle);
        isResumed = true;
        MobclickAgent.onResume(this);
        BusProvider.getInstance().register(this);
        mDownloadVideoUtil.startService(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        AnimationDrawable drawable = Utils.loadFrameAnimation(PlayerActivity.this);
        mBufferingSpin.setBackgroundDrawable(drawable);
        showBufferingAnimation(true);
    }

    @Override
    protected void onStop() {
        super.onStop();
        MobclickAgent.onEvent(this , "onStop" , mVid + "|" + mPos +"|" + mTitle);
        //通知服务器端，该视频播放了多久，时间单位为毫秒
        TVHttpClient.getInstance().reportPlayedTime(PlayerActivity.this ,mVid, getTotalWatchTime() / 1000, mPos);
        if (mDataItem != null) {
            mDataItem = null;
        }
        if (mBufferingSpin != null){
           Utils.removeFrameAnimation(mBufferingSpin);
        }
        if (mDelayedHandler != null){
            mDelayedHandler.removeCallbacksAndMessages(null);
        }
    }

    private void showBufferingAnimation(boolean show) {
        if (show) {
            mBufferingShowing = true;
            mBufferingIcon.setVisibility(View.VISIBLE);
            AnimationDrawable animationDrawable = (AnimationDrawable) mBufferingSpin.getBackground();
            if (animationDrawable != null){
                animationDrawable.start();
            }
        } else {
            mBufferingShowing = false;
            mBufferingSpin.clearAnimation();
            mBufferingIcon.setVisibility(View.GONE);
        }
    }

    private void disPlayTarget(int duration) {
        mRlTarget.setVisibility(View.VISIBLE);
        duration = duration / 1000 / 60;

        String hasLoseCalorie;
        if (TextUtils.isEmpty(getCurrentCalorie())) {
            hasLoseCalorie = "0.00";
        } else {
            hasLoseCalorie = getCurrentCalorie();
        }
        mTvTarget.setText(Html.fromHtml(getResources().getString(R.string.msg13) +
                mTarget + getResources().getString(R.string.msg14) +
                duration + getResources().getString(R.string.msg15) +
                hasLoseCalorie + getResources().getString(R.string.msg16)));

        TranslateAnimation ta = new TranslateAnimation(-600.0f, 0f, 0f, 0f);
        ta.setDuration(1000);
        ta.setFillAfter(true);
        mRlTarget.startAnimation(ta);
    }

    @Override
    public void onBackPressed() {
        if (isResumed){
            QuitVideoDialog quitVideoDialog = new QuitVideoDialog(this);
            quitVideoDialog.setCallBack(new QuitVideoDialog.QuitVideoDialogCallBack() {
                @Override
                public void continuePlay() {
                    if (mVideoView != null){
                        continuePlayVideo();
                        dismissMediaController();
                    }
                }

                @Override
                public void quitPlay() {
                    finish();
                }
            });
            quitVideoDialog.show();
        }
    }

    public void setAnimation() {
        ScaleAnimation scaleAnimation = new ScaleAnimation(0.1f, 1.0f, 0.1f, 1f,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
                0.5f);
        scaleAnimation.setDuration(2000);
        RotateAnimation rotateAnimation = new RotateAnimation(0, 360,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        rotateAnimation.setDuration(2000);
        AnimationSet animationSet = new AnimationSet(true);
        animationSet.addAnimation(scaleAnimation);
        animationSet.addAnimation(rotateAnimation);
        animationSet.start();
        animationSet.startNow();
        mErweima.startAnimation(animationSet);
    }

    double currentCar = 0;
    private Runnable updateCaloRunnable = new Runnable() {

        @Override
        public void run() {
            long time = getTotalWatchTime();
            double car ;
            car = TimeUtil.millToMin(time) * mKey * 60;
            if (time > 0 && car > currentCar){
                currentCar=car;
            }else{
                car=currentCar;
            }
            DecimalFormat df = new DecimalFormat("##000.0");
            mCalorie.setText(df.format(car));
            currentCar = car;
            if (mDelayedHandler != null){
                mDelayedHandler.postDelayed(updateCaloRunnable, DELAY);
            }
        }
    };

    public String getCurrentCalorie(){
        DecimalFormat df = new DecimalFormat("##000.0");
        return String.valueOf(df.format(currentCar));
    }

    private void postRunnable(){
        if (mDelayedHandler != null){
            mDelayedHandler.post(updateCaloRunnable);
        }
    }

    private void removeRunnable() {
        if (mDelayedHandler != null) {
            mDelayedHandler.removeCallbacks(updateCaloRunnable);
        }
    }


    private static class DelayHandler extends WKHandler<PlayerActivity> {

        public DelayHandler(PlayerActivity activity) {
            super(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            PlayerActivity mActivity =  mOuterClass.get();
            if ( mActivity == null) {
                return;
            }
            switch (msg.what) {
                case MSG_NOTIFICATION_TIME:
                    if (mActivity.mDownloadDialog != null) {
                        mActivity.mDownloadDialog.dismiss();
                    }
                    break;
                case MSG_VIDEO_DOWNLOADED_DIALOG_DISMISS:
                    if (mActivity.mVideoDownloadedDialog != null) {
                        mActivity.mVideoDownloadedDialog.dismiss();
                        mActivity.mVideoDownloadedDialog = null;
                    }
                    break;
            }
        }
    }

    @Subscribe
    public void onVideoDownloadStatusChange(DataItem dataItem) {
        if (dataItem == null) {
            return;
        }
        int status = dataItem.videoStatus;
        String vid = dataItem.videoVid;
        switch (status) {
            case DownloadVideoService.VIDEO_DOWNED:
                //判断是否是当前视频，且是否下载完成
                if (!TextUtils.isEmpty(vid) && vid.equals(mVid)) {
                    //视频下载完成
                    if (mVideoDownloadingDialog != null) {
                        mVideoDownloadingDialog.dismiss();
                    }
                    mVideoDownloadedDialog = new VideoDownloadedDialog(PlayerActivity.this);
                    mVideoDownloadedDialog.show();
                    mDelayedHandler.sendEmptyMessageDelayed(MSG_VIDEO_DOWNLOADED_DIALOG_DISMISS
                    ,MSG_SHORT_DELAY_TIME);
                    //播放本地视频
                    mDownloadFile = DownloadVideoUtils.getInstance()
                            .getVideoFilePath(Math.abs(mVid.hashCode()) ,PlayerActivity.this);
                    playLocalVideo(mDownloadFile);
                    isPlaylocalVideo = true;
                    showBufferingAnimation(false);
                }
                break;
            case DownloadVideoService.VIDEO_DOWNING:
                if (!TextUtils.isEmpty(vid) && vid.equals(mVid)) {
                    mVideoStatus = DownloadVideoService.VIDEO_DOWNING;
                    if (mVideoDownloadingDialog != null){
                        mVideoDownloadingDialog.setDownloadProgress(dataItem.videoProgress);
                    }else {
                        mVideoDownloadingDialog = new VideoDownloadingDialog(PlayerActivity.this);
                        mVideoDownloadingDialog.show();
                    }
                }
                break;
            case DownloadVideoService.VIDEO_DOWNLOAD_FAILURE:
                if (!TextUtils.isEmpty(vid) && vid.equals(mVid) && mVideoDownloadingDialog != null) {
                    mVideoDownloadingDialog.dismiss();
                }
                break;
            case DownloadVideoService.VIDEO_UNLOAD:
                break;
            default:
                break;
        }
    }
}
