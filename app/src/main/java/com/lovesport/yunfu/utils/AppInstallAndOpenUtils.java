package com.lovesport.yunfu.utils;

import android.content.Context;
import android.content.pm.PackageManager;
import com.lovesport.yunfu.javabean.DownloadAppItem;

/**
 * Created by lyn on 2015/2/3.
 */
public class AppInstallAndOpenUtils {

    /**
     * 该方法用于判断当前应用是否已经安装
     *
     * @param context
     * @param pkgName
     * @return
     */
    public static boolean isAppInstalled(Context context, String pkgName) {
        PackageManager pm = context.getPackageManager();
        boolean app_installed;
        try {
            pm.getPackageInfo(pkgName, PackageManager.GET_ACTIVITIES);
            app_installed = true;
        } catch (PackageManager.NameNotFoundException e) {
            app_installed = false;
        }
        return app_installed;
    }

    public static boolean tryInstallSilent(DownloadAppItem item) {
        if (!hasLocalAdbAndConnectable()) {
            return false;
        }
        String[] silentInstallCommands = {ADB_CONNECT,
                "adb -s 127.0.0.1:5555 shell pm install -r " + item.mFile.getAbsolutePath()};
        ShellUtils.CommandResult r = ShellUtils.execCommand(silentInstallCommands, false);
        return r.successMsg.toLowerCase().contains("success");
    }

    private static final String ADB_CONNECT = "adb connect 127.0.0.1";

    /**
     * test if adb routine is usable
     * will block thread
     *
     * @return true if has adb binary and is able to connect adbd through socket
     */
    public static boolean hasLocalAdbAndConnectable() {
        boolean mLocalAdbConnectable = false;
        ShellUtils.CommandResult r = ShellUtils.execCommand(ADB_CONNECT, false, true);
        mLocalAdbConnectable = r.successMsg.contains("connected");
        return mLocalAdbConnectable;
    }
}
