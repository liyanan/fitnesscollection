package com.lovesport.yunfu.dialog;

import android.content.Context;
import android.graphics.PixelFormat;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.lovesport.yunfu.R;
import com.lovesport.yunfu.javabean.DataItem;
import com.lovesport.yunfu.utils.DownloadVideoUtils;
import com.lovesport.yunfu.view.WukongLayout;
import com.umeng.analytics.MobclickAgent;


/**
 * Created by lyn on 2014/10/21.
 */
public class VideoDownloadDialog{

    private boolean mIsDownloaded = false;
    private Context mContext;
    private boolean mIsOOM = false;
    private View mContentView;
    private WindowManager mWindowManager;
    private boolean isShown;
    private DataItem mDataItem;
    private VideoDownloadDialogCallBack mCallBack;

    public VideoDownloadDialog(Context context, boolean isOOM, boolean isDownloaded, DataItem dataItem) {
        mIsDownloaded = isDownloaded;
        mContext = context;
        mDataItem = dataItem;
        mIsOOM = isOOM;
        initView();
    }

    private void initView(){

        if (mContext == null){
            return;
        }
        Button downloadvideo_button;
        if(mIsOOM){
            mContentView = View.inflate(mContext , R.layout.dialog_oom, null);
            downloadvideo_button = (Button) mContentView.findViewById(R.id.downloadvideo_button);
        }else {
            mContentView = View.inflate(mContext , R.layout.dialog_videoload, null);
            TextView downloadvideo = (TextView) mContentView.findViewById(R.id.downloadvideo);
            ImageView downloadvideo_pic = (ImageView) mContentView.findViewById(R.id.downloadvideo_pic);
            TextView downloadvideo_quit = (TextView) mContentView.findViewById(R.id.downloadvideo_quit);
            downloadvideo_button = (Button) mContentView.findViewById(R.id.downloadvideo_button);
            if (mIsDownloaded) {
                downloadvideo.setText(mContext.getResources().getString(R.string.downloadvideo_loaded));
                downloadvideo_pic.setImageDrawable(mContext.getResources().getDrawable(R.drawable.loaded));
                downloadvideo_button.setText(mContext.getResources().getString(R.string.downloadvideo_loaded_button));
            }
        }
        downloadvideo_button.requestFocus();

        downloadvideo_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mIsOOM) {
                    downloadVideo();
                    MobclickAgent.onEvent(mContext, "downloadVideo_low_memory_download");
                } else {
                    if (!mIsDownloaded) {
                        MobclickAgent.onEvent(mContext, "downloadVideo_unload_download");
                        downloadVideo();
                    }
                }
            }
        });
        WukongLayout layout = (WukongLayout) mContentView.findViewById(R.id.layout);
        layout.setOnKeyDownListen(new WukongLayout.onKeyEventListener() {
            @Override
            public void onKeyEvent(KeyEvent event) {
                if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
                    if (mContentView != null) {
                        if (mIsOOM){
                            MobclickAgent.onEvent(mContext , "downloadVideo_low_memory_back");
                        }else if (!mIsDownloaded){
                            MobclickAgent.onEvent(mContext ,"downloadVideo_unload_back");
                        }
                        dismiss();
                    }
                }
            }
        });
    }


    private void downloadVideo(){
        DownloadVideoUtils.getInstance().isDownloadOrOpenVideo(mDataItem);
        if (mCallBack != null){
            mCallBack.showDownloadingDialog();
        }
        dismiss();
    }

    public void show(){
        mWindowManager = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        layoutParams.flags = WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM ;
        layoutParams.type=WindowManager.LayoutParams.TYPE_SYSTEM_ALERT;
        layoutParams.format = PixelFormat.TRANSLUCENT;
        layoutParams.x = 0;
        layoutParams.y = 0;
        if (!isShown){
            try {
                mWindowManager.addView(mContentView , layoutParams);
                isShown = true;
                if (mIsOOM){
                    MobclickAgent.onEvent(mContext , "downloadVideo_low_memory_show");
                }else if (!mIsDownloaded){
                    MobclickAgent.onEvent(mContext , "downloadVideo_unload_show");
                }
            }catch (Exception e){
                isShown = false;
            }
        }
    }

    public void dismiss(){
        if (mContentView != null && isShown){
            try{
                mWindowManager.removeView(mContentView);
                isShown = false;
            }catch (Exception e){

            }
        }
    }

    public void setVideoDownloadDialogCallBack(VideoDownloadDialogCallBack callBack){
        mCallBack = callBack;
    }

    public interface VideoDownloadDialogCallBack{
        public void showDownloadingDialog();
    }

}
