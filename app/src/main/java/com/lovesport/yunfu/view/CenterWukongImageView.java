package com.lovesport.yunfu.view;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.lovesport.lc.AutoTextView;
import com.lovesport.yunfu.R;

/**
 * Created by lyn on 2014/12/8.
 */
public class CenterWukongImageView extends BaseWukongImageView {

    public CenterWukongImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void initView() {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.view_wukongimageview_center, this);
        setWillNotDraw(false);
        findViews();
        setValues();
    }

    private void findViews() {

        mAlbumPic = (ImageView) findViewById(R.id.album_details_pic);
        mTextContent = (ViewGroup) findViewById(R.id.wkImageView_text_content);
        mTextContent.setBackgroundColor(mTextContentBg);
        setTextContentHeight(mTextContent, mTextContentHeight);
        mAlbumName = (AutoTextView) findViewById(R.id.album_name);
        mAlbumNum = (AutoTextView) findViewById(R.id.album_num);
        mVideoDownloaded = (TextView) findViewById(R.id.downloaded);
        mVideoDownloaded.setVisibility(GONE);
    }

    private void setValues() {

        mAlbumName.setTextColor(mTitleColor);
        mAlbumName.setAutoTextSize(mTitleSize);
        mAlbumNum.setAutoTextSize(mOtherDesSize);
        mAlbumNum.setTextColor(mOtherDesColor);
        mAlbumNum.setVisibility(GONE);
        if (!TextUtils.isEmpty(mTitle)) {
            mAlbumName.setText(mTitle);
        }
        if (!TextUtils.isEmpty(mOtherDes)) {
            mAlbumNum.setText(mOtherDes);
        }
    }
}
