package com.lovesport.yunfu;

/**
 * Created by lyn on 2014/12/11.
 */
public interface Constant {

    public final static String SHAREDPREFERENCE = "config";
    public final static String POS = "pos";
    public final static String TYPE = "type";
    public final static String NAME = "name";
    public final static String KEY = "key";
    public final static String HAS_MORE = "key";
    public final static String INDEX = "index";
    public final static String JIAN_FEI = "jianfei";
    public final static String YU_JIA = "yujia";
    public final static String YANG_SHENG = "yangsheng";
    public final static String ER_TONG = "ertong";
    public final static String JIAN_MEI = "jianmei";
    public final static String SU_XING = "suxing";
    public final static String QI_CAI = "qicai";
    public static final String TODAY_FIRST_PLAYVIDEO_TIME = "todayFirstPlayVideoTime";
    public static final String QQ = "428435677";
    public static final String UA = "wk/ttjs";
    public static final String VIDEO_FOLDER_NAME = "downloadedVideo";
    public static final String FILESPACE = "fileSpace";
    public static final String VIDEO_STATUS = "videoStatus";
    public static final String TTJS = "ttjs";
    public static final String APP_FOLDER_NAME = "downloadedApp";
}
