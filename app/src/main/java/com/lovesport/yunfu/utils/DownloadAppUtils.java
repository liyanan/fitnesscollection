package com.lovesport.yunfu.utils;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;
import com.lovesport.yunfu.Constant;
import com.lovesport.yunfu.javabean.DownloadAppItem;
import com.lovesport.yunfu.service.DownloadAppService;

/**
 * Created by Administrator on 2014/10/8.
 */
public class DownloadAppUtils implements Constant {

    private DownloadAppService mService;
    private boolean mBound;

    private DownloadAppUtils() {

    }

    private static DownloadAppUtils mDownloadVideoUtils;

    public static DownloadAppUtils getInstance() {
        if (mDownloadVideoUtils == null) {
            synchronized (DownloadAppUtils.class){
                if (mDownloadVideoUtils == null){
                    mDownloadVideoUtils = new DownloadAppUtils();
                }
            }
        }
        return mDownloadVideoUtils;
    }

    public void startService(Context context) {

        Intent i = new Intent(context, DownloadAppService.class);
        context.startService(i);
        if (!mBound) {
            try {
                context.bindService(i, mConnection, Context.BIND_AUTO_CREATE);
            } catch (Exception ignored) {

            }
        }
    }

    /**
     * 解除绑定
     */
    public void unbindService(Context context) {
        if (mBound && context != null) {
            try {
                context.unbindService(mConnection);
                mBound = false;
            } catch (Exception ignored) {

            }
        }
    }

    /**
     * 用于下载的监听
     */

    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            DownloadAppService.LocalBinder binder = (DownloadAppService.LocalBinder) service;
            mService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };

    public void downloadOrOpenApp(Context context ,DownloadAppItem item){

        if (AppInstallAndOpenUtils.isAppInstalled(context ,item.mPkg)){
            Log.i("mandy" ,"安装啦~~~~~~");
            //daka ~~~~
        }else {
            Log.i("mandy" ,"下载去咯~~~");
            if (mService != null){
                mService.startDownloadAPP(item);
            }
        }
    }
}
