package com.lovesport.yunfu.javabean;

import java.util.ArrayList;

/**
 * Created by lyn on 2014/12/11.
 */
public class Videos {

    public String videoPicUrl;
    public String videoPicDic;
    public String videoJianjie;
    public String videoApi;
    public ArrayList<VideoDetails > videoDetailses = new ArrayList<>();

    public String getVideoPic(int position) {

        if (videoDetailses != null && videoDetailses.size() > position) {
            return videoPicUrl +"/"+ videoPicDic +"/"+ videoDetailses.get(position).videoPicName;
        }
        return null;
    }
}
