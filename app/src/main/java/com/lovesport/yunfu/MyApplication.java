package com.lovesport.yunfu;


import android.app.Application;

import com.lovesport.lc.LC;
import com.lovesport.yunfu.utils.OmniStorage;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.LRULimitedMemoryCache;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;
import com.wukongtv.http.WKAsyncHttpClient;
import com.wukongtv.http.WKDiskCache;

import java.io.File;

/**
 * Created by lyn on 2014/12/11.
 */
public class MyApplication extends Application  {

    @Override
    public void onCreate() {
        super.onCreate();

        OmniStorage storage = new OmniStorage(getApplicationContext());
        storage.init();
        File cacheDir = storage.getCacheDir();

        //对Wkhttpclient进行全局初始化
        WKDiskCache diskCache = new WKDiskCache(cacheDir);
        WKAsyncHttpClient.getInstance().init(diskCache);
        WKAsyncHttpClient.getInstance().setUserAgent(Constant.UA);

        //对ImageLoader进行初始化
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
                .threadPoolSize(3) //线程池内加载的数量
                .threadPriority(Thread.NORM_PRIORITY - 2)
                .denyCacheImageMultipleSizesInMemory()//拒绝缓存多个图片
                .discCacheFileNameGenerator(new Md5FileNameGenerator()) //保存的时候uri名称使用md5加密
                .tasksProcessingOrder(QueueProcessingType.LIFO)//设置图片下载和显示的工作对列  后进先出
                .imageDownloader(new BaseImageDownloader(this , 5 * 1000 , 30* 1000))//connectTimeout为5秒，readTimeout为30秒。
                .memoryCache(new LRULimitedMemoryCache(1024 * 1024))//缓存策略为lru算法
                .memoryCacheSize(1024 * 1024) // 1M
                .discCache(new UnlimitedDiscCache(cacheDir))
                .build();

        ImageLoader.getInstance().init(config);
        LC.init(this);

    }

}
