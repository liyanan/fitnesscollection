package com.lovesport.yunfu.service;

import android.app.Service;
import android.content.Intent;
import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.FileAsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.lovesport.yunfu.Constant;
import com.lovesport.yunfu.bus.BusProvider;
import com.lovesport.yunfu.javabean.DownloadAppItem;
import com.lovesport.yunfu.utils.AppInstallAndOpenUtils;
import com.lovesport.yunfu.utils.OmniStorage;
import org.apache.http.Header;
import java.io.File;
import java.util.HashMap;

public class DownloadAppService extends Service implements Constant {

    public static final int DOWNLOAD_STATE_LOADING = 0; //正在下载
    public static final int DOWNLOAD_STATE_LOADING_FAILD = 5; //下载失败了~~
    public static final int DOWNLOAD_STATE_INSTALL_SYSTEM_UI = 1; //无法自动安装，弹框~~~
    public static final int DOWNLOAD_STATE_INSTALL_ING = 2; //正在安装
    public static final int DOWNLOAD_STATE_INSTALL_SUCCESS = 3; //安装成功了~
    public static final int DOWNLOAD_STATE_INSTALL_FAILD = 4;//安装失败了~

    private final IBinder mBinder = new LocalBinder();
    private static AsyncHttpClient mClient = new AsyncHttpClient();
    private int mNumBoundClients = 0;

    private HashMap<String,String> appFlags = new HashMap<>();
    /**
     * Class used for the client Binder.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with IPC.
     */
    public class LocalBinder extends Binder {
        public DownloadAppService getService () {
            return DownloadAppService.this;
        }
    }

    @Override
    public IBinder onBind (Intent intent) {
        mNumBoundClients++;
        return mBinder;
    }

    @Override
    public boolean onUnbind (Intent intent) {
        mNumBoundClients--;
        return super.onUnbind(intent);
    }

    /**
     *
     * @param url
     * @param responseHandler
     */
    public  void get (String url, AsyncHttpResponseHandler responseHandler) {
        mClient.setTimeout(20000);
        mClient.setEnableRedirects(true , true);
        mClient.get(url, responseHandler);
    }

    /**
     *
     * @param url
     * @param responseHandler
     */
    public  void get (String url, FileAsyncHttpResponseHandler responseHandler) {
        mClient.setTimeout(20000);
        mClient.setEnableRedirects(true,true);
        mClient.get(url, responseHandler);
    }

    /**
     * @param url
     * @param params
     * @param responseHandler
     */
    public  void get (String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        mClient.setTimeout(20000);
        mClient.setEnableRedirects(true,true);
        mClient.get(url, params, responseHandler);
    }

    public void startDownloadAPP (final DownloadAppItem item){

        final File downloadFile ,desFile;
        final String name = item.mName;
        //todo 下载视频的真实地址
        String url =  item.mUrl;
        final String pkg = item.mPkg;

        appFlags.put(pkg , name);
        String downloadedFileName = Math.abs(pkg.hashCode()) + ".ttjs.temp";
        downloadFile = OmniStorage.getPublicStorageFile(APP_FOLDER_NAME, downloadedFileName, DownloadAppService.this);
        String desFileName =  Math.abs(pkg.hashCode()) + ".apk";
        desFile = OmniStorage.getPublicStorageFile(APP_FOLDER_NAME, desFileName, DownloadAppService.this);
        item.mFile = desFile;
        get(url, new FileAsyncHttpResponseHandler(downloadFile) {

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
                Log.i("mandy" ,"onFailure");
                if (file.exists()){
                    file.delete();
                }
                item.mStatus = DOWNLOAD_STATE_LOADING_FAILD;
                BusProvider.getInstance().post(item);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, File file) {
                file.renameTo(desFile);
                Log.i("mandy" ,"onSuccess");
                //todo 下载成功了以后去安装咯~~~
                installApk(item);
            }

            @Override
            public void onStart(){
                Log.i("mandy" ,"onStart");
                //todo 开始下载啦~
            }

            @Override
            public void onProgress(int bytesWritten, int totalSize) {
                super.onProgress(bytesWritten, totalSize);
                Log.i("mandy" ,"onProgress");
                item.mStatus = DOWNLOAD_STATE_LOADING;
                bytesWritten = bytesWritten / 1024;
                totalSize = totalSize / 1024;
                item.mProgress = bytesWritten * 100/ totalSize ;
                BusProvider.getInstance().post(item);
            }

            @Override
            public void onFinish() {
                if (appFlags.containsKey(pkg)) {
                    appFlags.remove(pkg);
                    if (appFlags.size() == 0 && mNumBoundClients == 0) {
                        stopSelf();
                    }
                }
            }
        });
    }


    @Override
    public int onStartCommand (Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onCreate () {
        super.onCreate();
    }

    @Override
    public void onDestroy () {
        super.onDestroy();
    }

    private void installApk(DownloadAppItem item){

        item.mStatus = DOWNLOAD_STATE_INSTALL_ING;
        BusProvider.getInstance().post(item);

        if (AppInstallAndOpenUtils.tryInstallSilent(item)) {
            Log.i("mandy" ,"adb 安装");
            item.mStatus = DOWNLOAD_STATE_INSTALL_SUCCESS;
            BusProvider.getInstance().post(item);
            // if silent install succeed, remove apk file
            item.mFile.delete();
        } else {
            Log.i("mandy" ,"默认安装");
            Intent installIntent = new Intent(Intent.ACTION_VIEW);
            installIntent.setDataAndType(Uri.fromFile(item.mFile),
                    "application/vnd.android.package-archive");
            installIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            try {
                startActivity(installIntent);
            } catch (Exception ignore) {

            }
            item.mStatus = DOWNLOAD_STATE_INSTALL_SYSTEM_UI;
            BusProvider.getInstance().post(item);
        }
    }
}

