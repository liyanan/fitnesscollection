package com.lovesport.yunfu.view;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.lovesport.lc.LC;
import com.lovesport.yunfu.R;

/**
 * Created by lyn on 2014/10/23.
 */
public class WukongProgressBar extends View{

        private Paint mPaint ,bgPaint ,cirPaint;
        private int mProgress;// 表示进度
        private RectF mRect;
        private float mDiameter; // Diameter英文为直径，在该View中要绘制圆环，圆环由两个圆形确定（大圆和小圆），这个整形值表示小圆直径。
        private float mWidth ;// 这个值表示圆环的宽度的2倍（大圆直径-小圆直径）

        private  int defaultColor; //进度条背景颜色
        private  int processColor; //进度条进度颜色
        private RectF rectF ;
        private RectF bgRecF;
        private float mDistance;
        private float mBgwidth;

        public WukongProgressBar(Context context, AttributeSet attrs) {
            super(context, attrs);

            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.WukongProgressBar);
            mWidth = LC.getScaledLength(a.getDimension(R.styleable.WukongProgressBar_progressWidth, 0));
            mDiameter = LC.getScaledLength(a.getDimension(R.styleable.WukongProgressBar_Diameter, 0));
            mDistance = LC.getScaledLength(a.getDimension(R.styleable.WukongProgressBar_distance, 0));
            mBgwidth = LC.getScaledLength(a.getDimension(R.styleable.WukongProgressBar_bgwidth, 0));
            Log.v("mandy" ,"mWidth :" + mWidth + "mDiameter :" + mDiameter + " mDistance:" + mDistance + "mBgwidth :" + mBgwidth);
            init();
        }

        private void init() {

            defaultColor = getResources().getColor(R.color.defaultColor);
            processColor = getResources().getColor(R.color.processColor);

            Paint p = new Paint();
            p.setStyle(Paint.Style.STROKE);
            p.setAntiAlias(true);
            p.setStrokeWidth(0.5F * mWidth);
            p.setStrokeCap(Paint.Cap.ROUND);
            p.setColor(defaultColor);
            mPaint = p;

            bgPaint = new Paint();
            bgPaint.setStyle(Paint.Style.FILL);
            bgPaint.setAntiAlias(true);
            bgPaint.setStrokeCap(Paint.Cap.ROUND);
            bgPaint.setColor(getResources().getColor(com.lovesport.fitCommon.R.color.wukongprogressbarbg));

            cirPaint = new Paint();
            cirPaint.setStyle(Paint.Style.STROKE);
            cirPaint.setAntiAlias(true);
            cirPaint.setStrokeCap(Paint.Cap.ROUND);
            cirPaint.setStrokeWidth(2);
            cirPaint.setColor(Color.WHITE);

            int rightTop = (int) (mWidth / 2);//这个值就是圆环宽度（大圆半径-小圆半径）

            int distance = (int) (rightTop + mBgwidth);
            int rightDistance = (int) (distance + mDiameter);

            mRect = new RectF( distance , distance, rightDistance, rightDistance);
            rectF = new RectF(distance - mDistance , distance - mDistance ,rightDistance + mDistance ,rightDistance + mDistance);
            bgRecF = new RectF(rightTop , rightTop  , rightDistance + mBgwidth, rightDistance + mBgwidth);
            mProgress = 0;
        }

        @Override
        protected void onDraw(Canvas canvas) {

            canvas.drawArc( bgRecF , 0 ,360 ,false ,bgPaint);
            canvas.drawArc( rectF , 135 ,270 ,false ,cirPaint);

            Paint paint = mPaint;
            //如果mProgress<360,则圆形进度条还未旋转完成，则用0x7f的透明度绘制一个完整的圆形作为进度条背景
            //注意要先绘制背景条，再绘制进度条，因为后绘制的会覆盖在先绘制的上面
            if (mProgress < 360) {
                paint.setAlpha(0x7f);
                paint.setColor(defaultColor);
                canvas.drawArc(mRect, 135, 270, false, paint);
            }
            if (mProgress != 0) {
                float degree = 270.0f * mProgress / 360;
                paint.setAlpha(0xff);
                paint.setColor(processColor);
                //drawArc(RectF oval, float startAngle, float sweepAngle, boolean useCenter, Paint paint)
                //该方法参数：
                //oval表示绘制椭圆的矩形边界；
                //startAngle表示起始角度；
                //useCenter
                //paint表示要使用的画笔；
                canvas.drawArc(mRect, 135, degree, false, paint);
            }
        }

        @Override
        protected final void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
            setMeasuredDimension(widthMeasureSpec ,heightMeasureSpec);
        }

        public void setProgress(int p) {
            mProgress = p;
            invalidate();
        }
}
