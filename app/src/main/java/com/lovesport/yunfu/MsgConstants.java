package com.lovesport.yunfu;

/**
 * 
 * Created by zhangge on 13-12-17.
 */
public class MsgConstants {

    public static final String STATUS_ENQUEUED = "enqueued";
    public static final String STATUS_INQUEUE = "inqueue";
    public static final String STATUS_INSTALLED = "installed";
    public static final String STATUS_ERROR = "error";
    public static final String STATUS_ILLEGAL_URL = "illegal url";
}
