package com.lovesport.yunfu.http;

import android.content.Context;
import android.util.Log;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.lovesport.yunfu.utils.UmengUtils;
import com.wukongtv.http.WKAsyncHttpClient;
import com.wukongtv.http.WKResponseHandler;

import org.apache.http.Header;

/**
 * Created by lyn on 2014/12/19.
 */
public class TVHttpClient {

    private static final String HOST = "http://api.tvjianshen.com";

    private static TVHttpClient mTvHttpClient;

    private TVHttpClient(){

    }

    public static TVHttpClient getInstance(){
        if (mTvHttpClient  == null){
            synchronized (TVHttpClient.class){
                if (mTvHttpClient == null){
                    mTvHttpClient = new TVHttpClient();
                }
            }
        }
        return mTvHttpClient;
    }

    public void requestAll(Context context ,String type , WKResponseHandler handler){
        String url  = HOST + "/jspt/fenlei/getapp?type="+type + "&ver=" + UmengUtils.getVersion(context) +"&qudao=" + UmengUtils.getUmengValue(context);
        Log.i("mandy" ,"url :" + url);
        WKAsyncHttpClient.getInstance().request( url , null,  handler , true);
    }

    public void requestVideos(Context context ,String type , String pos , WKResponseHandler handler){
        WKAsyncHttpClient.getInstance().request( HOST + "/jspt/shipin/get?type="+type+"&pos="+pos+"&ver=" + UmengUtils.getVersion(context) +"&qudao=" + UmengUtils.getUmengValue(context) , null,  handler , true);
    }

    public void requestVideoDownloadUrl(String url , AsyncHttpResponseHandler handler){
        WKAsyncHttpClient.getInstance().get( url , null , handler,false);
    }

    public void reportPlayedTime(Context context ,String vid, long time, String pos){
        WKAsyncHttpClient.getInstance().get(HOST + "/jspt/shipin/sprp?vid=" + vid + "&time=" + time + "&p=ttjs" + "&pos=" + pos  + "&ver=" + UmengUtils.getVersion(context) +"&qudao=" + UmengUtils.getUmengValue(context), null, defaultHandler, false);
    }

    public void reportPlayError(Context context ,String vid,  String pos){
        WKAsyncHttpClient.getInstance().get(HOST + "/jspt/shipin/errp?vid=" + vid  + "&p=ttjs" + "&pos=" + pos  + "&ver=" + UmengUtils.getVersion(context) +"&qudao=" + UmengUtils.getUmengValue(context), null, defaultHandler, false);
    }

    private AsyncHttpResponseHandler defaultHandler = new AsyncHttpResponseHandler() {
        @Override
        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

        }

        @Override
        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

        }
    };
}
