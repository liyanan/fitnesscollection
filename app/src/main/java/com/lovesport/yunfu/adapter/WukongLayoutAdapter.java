package com.lovesport.yunfu.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.lovesport.fitCommon.PlayerConstant;
import com.lovesport.yunfu.Constant;
import com.lovesport.yunfu.R;
import com.lovesport.yunfu.activity.PlayerActivity;
import com.lovesport.yunfu.javabean.VideoDetails;
import com.lovesport.yunfu.javabean.Videos;
import com.lovesport.yunfu.service.DownloadVideoService;
import com.lovesport.yunfu.utils.AppInstallAndOpenUtils;
import com.lovesport.yunfu.utils.DownloadVideoUtils;
import com.lovesport.yunfu.view.BaseWukongImageView;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

import java.util.ArrayList;

/**
 * Created by lyn on 2014/12/10.
 */
public class WukongLayoutAdapter extends WKPageAdapter implements PlayerConstant, Constant {

    private ArrayList<VideoDetails> mItems = new ArrayList();
    private Context mContext;
    private Videos mVideos;
    private DisplayImageOptions mOptions;
    private String mKey;
    private String mPos;
    private boolean hasRecmd;


    public WukongLayoutAdapter(Context context, ArrayList arrayList, Videos videos, String key, String pos) {
        mItems = arrayList;
        mContext = context;
        mVideos = videos;
        mKey = key;
        mPos = pos;

        hasRecmd = true;
        mOptions = new DisplayImageOptions.Builder()
                .showImageForEmptyUri(R.drawable.placeholder)
                .showImageOnFail(R.drawable.placeholder)
                .showImageOnLoading(R.drawable.placeholder)
                .cacheInMemory(false)
                .cacheOnDisc(true)
                .bitmapConfig(Bitmap.Config.ARGB_8888)
                .build();
        if (mItems == null || mContext == null) {
        }
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public View getView(int position, ViewGroup parent, View convertView) {

        if (convertView == null){
            convertView = LayoutInflater.from(mContext).inflate(R.layout.activity_album_details_image, null);
        }
        convertView.setTag(position);
        convertView.setOnClickListener(onClickListener);
        onBindData(position, convertView);

        return convertView;
    }

    @Override
    public void onBindData(int position, View contentView) {
        if (contentView instanceof BaseWukongImageView) {
            String onLinePlayedCount = mItems.get(position).videoPlayedCount;
            BaseWukongImageView imageView = (BaseWukongImageView) contentView;
            imageView.setVideoDownloaded(false);
            if (!TextUtils.isEmpty(onLinePlayedCount) && Integer.valueOf(onLinePlayedCount) > 10000) {
                onLinePlayedCount = mContext.getString(R.string.video_playedCount, onLinePlayedCount);
                imageView.setOtherDes(onLinePlayedCount);
            }
            String videoYouKuId = mItems.get(position).videoYouKuId;
            if (!TextUtils.isEmpty(videoYouKuId)) {
                if (DownloadVideoUtils.getInstance().isVideoDownloaded(Math.abs(videoYouKuId.hashCode()) , mContext)) {
                    imageView.setVideoDownloaded(true);
                    mItems.get(position).videoStatus = DownloadVideoService.VIDEO_DOWNED;
                }
            }
            imageView.setAlbumTitle(mItems.get(position).videoTitle);
            imageView.setImageView(mVideos.getVideoPic(position), mOptions);
        }
    }

    @Override
    public void onUnBindData(int position, View contentView) {
        if (contentView instanceof BaseWukongImageView) {
            BaseWukongImageView imageView = (BaseWukongImageView) contentView;
            imageView.releaseImageViewDrawable();
        }
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int position = (int) view.getTag();
            VideoDetails details = mItems.get(position);
            if (position == 0 && hasRecmd){
                openOrDownloadTtjs();
            }else {
                Intent intent = new Intent(mContext, PlayerActivity.class);
                intent.putExtra(PLAY_URI_API, mVideos.videoApi);
                intent.putExtra(VIDEO_ID, details.videoYouKuId);
                intent.putExtra(PLAY_TITLE, details.videoTitle);
                intent.putExtra(PLAY_KEY, mKey);
                intent.putExtra(POS, mPos);
                intent.putExtra(VIDEO_STATUS, details.videoStatus);
                mContext.startActivity(intent);
            }
        }
    };

    private void openOrDownloadTtjs(){
        if (AppInstallAndOpenUtils.isAppInstalled(mContext ,
                mContext.getResources().getString(R.string.package_name))){
            Log.i("mandy" ,"安装了，去打开~~~");
            //todo 安装了，去打开~~~
        }else {
            //todo 未安装，去下载咯~~
            Log.i("mandy" ,"未安装，去下载咯~~");
        }
    }
}
