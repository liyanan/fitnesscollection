package com.lovesport.yunfu.dialog;

import android.content.Context;
import android.graphics.PixelFormat;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.lovesport.yunfu.R;
import com.lovesport.yunfu.utils.Utils;
import com.lovesport.yunfu.view.WukongLayout;
import com.umeng.analytics.MobclickAgent;

/**
 * Created by lyn on 2014/12/17.
 */
public class QuitVideoDialog {

    private WindowManager mWindowManager;
    private Context mContext;
    private boolean isShown;
    private View mContentView;
    private QuitVideoDialogCallBack mQuitVideoDialogCallBack;
    private ImageView mQuitVideoDialogImage;
    private WindowManager.LayoutParams mLayoutParams;


    public QuitVideoDialog(Context context) {
        mContext = context;
        initView();
    }
    private void initView(){

        mContentView = View.inflate(mContext , R.layout.dialog_quit_video , null);
        WukongLayout layout = (WukongLayout) mContentView.findViewById(R.id.dialog_quitVideo_layout);
        mQuitVideoDialogImage = (ImageView) mContentView.findViewById(R.id.dialog_quitVideo_image);
        Utils.setImageDrawable(mQuitVideoDialogImage, mContext, R.drawable.dialog_quit_video_boy);
        layout.setOnKeyDownListen(new WukongLayout.onKeyEventListener() {
            @Override
            public void onKeyEvent(KeyEvent event) {
                if (event.getKeyCode() == KeyEvent.KEYCODE_BACK ){
                    MobclickAgent.onEvent(mContext , "quit_video_dialog_back" );
                    if (mContentView != null){
                        dismiss();
                    }
                }
            }
        });

        TextView continuePlay = (TextView) mContentView.findViewById(R.id.dialog_quitVideo_continue);
        TextView quitPlay = (TextView) mContentView.findViewById(R.id.dialog_quitVideo_quit);
        continuePlay.setOnClickListener(onClickListener);
        quitPlay.setOnClickListener(onClickListener);
    }


    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            if (v.getId() == R.id.dialog_quitVideo_continue){
                dismiss();
                if (mQuitVideoDialogCallBack != null){
                    mQuitVideoDialogCallBack.continuePlay();
                }
                MobclickAgent.onEvent(mContext , "quit_video_dialog_continue");
            }else if(v.getId() == R.id.dialog_quitVideo_quit){

                if (mWindowManager != null && mLayoutParams != null){
                    mLayoutParams.windowAnimations = 0;
                    mWindowManager.updateViewLayout(mContentView , mLayoutParams);
                }
                dismiss();
                if (mQuitVideoDialogCallBack != null){
                    mQuitVideoDialogCallBack.quitPlay();
                }
                MobclickAgent.onEvent(mContext , "quit_video_dialog_quit");
            }
        }
    };

    public interface QuitVideoDialogCallBack {
        public void continuePlay();
        public void quitPlay();
    }

    public void setCallBack(QuitVideoDialogCallBack callBack){
        this.mQuitVideoDialogCallBack = callBack;
    }

    public void show() {
        mWindowManager = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
        mLayoutParams = new WindowManager.LayoutParams();
        mLayoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;
        mLayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        mLayoutParams.flags = WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM ;
        mLayoutParams.type=WindowManager.LayoutParams.TYPE_SYSTEM_ALERT;
        mLayoutParams.format = PixelFormat.TRANSLUCENT;
        mLayoutParams.windowAnimations = R.style.anim_view;
        mLayoutParams.x = 0;
        mLayoutParams.y = 0;
        if (!isShown){
            try {
                mWindowManager.addView(mContentView , mLayoutParams);
                isShown = true;
                MobclickAgent.onEvent(mContext , "quit_video_dialog_show");
            }catch (Exception e){
                isShown = false;
            }
        }
    }

    public void dismiss() {
        if (mContentView != null && isShown){
            try{
                mWindowManager.removeView(mContentView);
                isShown = false;
            }catch (Exception ignored){
                isShown = true;
            }
        }
    }
}
