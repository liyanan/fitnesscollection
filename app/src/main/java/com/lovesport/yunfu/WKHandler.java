package com.lovesport.yunfu;

import android.os.Handler;

import java.lang.ref.WeakReference;

/**
 * Created by lyn on 2015/1/12.
 */
public class WKHandler<T> extends Handler {

    public WeakReference<T>  mOuterClass;

    public WKHandler(T t){
        mOuterClass = new WeakReference<>(t);
    }
}
