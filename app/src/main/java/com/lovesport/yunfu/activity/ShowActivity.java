package com.lovesport.yunfu.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.lovesport.yunfu.Constant;
import com.lovesport.yunfu.R;
import com.lovesport.yunfu.bus.BusProvider;
import com.lovesport.yunfu.http.TVHttpClient;
import com.lovesport.yunfu.javabean.Album;
import com.lovesport.yunfu.javabean.DownloadAppItem;
import com.lovesport.yunfu.service.DownloadAppService;
import com.lovesport.yunfu.utils.AppInstallAndOpenUtils;
import com.lovesport.yunfu.utils.DownloadAppUtils;
import com.lovesport.yunfu.utils.JsonParser;
import com.lovesport.yunfu.utils.Utils;
import com.squareup.otto.Subscribe;
import com.wukongtv.http.WKResponseHandler;
import org.json.JSONObject;

public class ShowActivity extends Activity implements View.OnClickListener, Constant {

    private static final String TTJS_PKG = "com.tvjianshen.tvfit";
    private RelativeLayout mRootLayout;
    private boolean isInstalled;
    private Button mInstallButton;
    private Album mAlbum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_show2);
        initView();
        TVHttpClient.getInstance().requestAll(this, "yunfu", new WKResponseHandler() {
            @Override
            public void onSuccess(int status, JSONObject responseBody, boolean fromCache) {
                mAlbum = JsonParser.parseAlbum(responseBody);
            }

            @Override
            public void onFailure(int status, String reason) {

            }
        });
    }

    private void initView() {
        isInstalled = AppInstallAndOpenUtils.isAppInstalled(this, TTJS_PKG);
        mRootLayout = (RelativeLayout) findViewById(R.id.root);
        mInstallButton = (Button) findViewById(R.id.button);
        //如果已安装
        int resId;
        if (isInstalled) {
            resId = R.drawable.btn_open_ttjs;
        } else {
            resId = R.drawable.btn_download;
        }
        mInstallButton.setBackgroundDrawable(new BitmapDrawable(Utils.readBitmap(this, resId)));
        mInstallButton.setOnClickListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Bitmap bitmap = Utils.readBitmap(this, R.drawable.poster);
        mRootLayout.setBackgroundDrawable(new BitmapDrawable(bitmap));
    }

    @Override
    protected void onResume() {
        super.onResume();
        DownloadAppUtils.getInstance().startService(this);
        BusProvider.getInstance().register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        DownloadAppUtils.getInstance().unbindService(this);
        BusProvider.getInstance().unregister(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Utils.removeBackground(mRootLayout);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button:
                DownloadAppUtils.getInstance().downloadOrOpenApp(ShowActivity.this,
                        new DownloadAppItem(null, "http://down.tvjianshen.com/jspt/ttjs-ceshi-release.apk"
                                , "com.tvjianshen.tvfit", "天天健身"));
//                    mInstallManager.downloadAndInstall("http://down.tvjianshen.com/jspt/ttjs-ceshi-release.apk" , mUiHandler , getResources().getString(R.string.ttjs));
                break;
            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (mAlbum != null) {
            jumpToNextActivity(mAlbum.hasMore, mAlbum.pos, mAlbum.type, mAlbum.title, mAlbum.key, AlbumDetailsActivity.class);
        } else {
            Toast.makeText(this, getString(R.string.onloadfailure), Toast.LENGTH_SHORT).show();
        }
    }

    private void jumpToNextActivity(String hasMore, String pos, String type, String name, String key, Class clazz) {
        Intent intent = new Intent(ShowActivity.this, clazz);
        intent.putExtra(HAS_MORE, hasMore);
        intent.putExtra(POS, pos);
        intent.putExtra(TYPE, type);
        intent.putExtra(NAME, name);
        intent.putExtra(KEY, key);
        startActivity(intent);
    }

    boolean isStart = false;

    @Subscribe
    public void onTtjsDownloadStatusChange(DownloadAppItem downloadItem) {
        if (downloadItem == null) {
            return;
        }
        int status = downloadItem.mStatus;
        switch (status) {
            case DownloadAppService.DOWNLOAD_STATE_LOADING:
                if (!isStart) {
                    mInstallButton.setBackgroundDrawable(new BitmapDrawable(
                            Utils.readBitmap(ShowActivity.this, R.drawable.btn_downloading)));
                    isStart = true;
                }
                //正在下载中
                Log.i("mandy", "正在下载中" + downloadItem.mProgress);
                break;
            case DownloadAppService.DOWNLOAD_STATE_INSTALL_FAILD:
                //下载失败
                Log.i("mandy", "下载失败");
                break;
            case DownloadAppService.DOWNLOAD_STATE_INSTALL_ING:
                mInstallButton.setBackgroundDrawable(new BitmapDrawable(
                        Utils.readBitmap(ShowActivity.this, R.drawable.btn_installing)));
                //正在安装
                Log.i("mandy", "正在安装");
                break;
            case DownloadAppService.DOWNLOAD_STATE_INSTALL_SUCCESS:
                mInstallButton.setBackgroundDrawable(new BitmapDrawable(
                        Utils.readBitmap(ShowActivity.this, R.drawable.btn_open_ttjs)));
                //安装成功
                Log.i("mandy", "安装成功");
                break;
            case DownloadAppService.DOWNLOAD_STATE_INSTALL_SYSTEM_UI:
                //不支持自动安装，在安装界面操作
                Log.i("mandy", "不支持自动安装，在安装界面操作");
                break;
            default:
                break;
        }
    }
}
