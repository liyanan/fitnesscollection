package com.lovesport.yunfu.javabean;


import com.lovesport.yunfu.service.DownloadVideoService;

/**
 * Created by lyn on 2014/12/11.
 */
public class VideoDetails {

        public String videoTitle;
        public String videoPicName;
        public String videoPlayedCount;
        public String videoYouKuId;
        public int videoStatus = DownloadVideoService.VIDEO_UNLOAD;


    public VideoDetails(){

    }

    public VideoDetails(String videoTitle ,String videoPicName ,String videoPlayedCount ,String videoYouKuId ,int videoStatus){
        this.videoTitle = videoTitle;
        this.videoPlayedCount = videoPlayedCount;
        this.videoPicName = videoPicName;
        this.videoStatus = videoStatus;
        this.videoYouKuId = videoYouKuId;
    }
}
