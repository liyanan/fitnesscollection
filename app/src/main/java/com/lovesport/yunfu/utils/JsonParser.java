package com.lovesport.yunfu.utils;


import android.content.Context;
import android.util.Log;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.TextHttpResponseHandler;
import com.lovesport.yunfu.javabean.Album;
import com.lovesport.yunfu.javabean.VideoDetails;
import com.lovesport.yunfu.javabean.Videos;
import com.lovesport.yunfu.service.DownloadVideoService;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;


/**
 * Created by lyn on 2014/12/11.
 */
public class JsonParser {

    public static Album parseAlbum(JSONObject jsonObj) {

        Album album = null;
        if (jsonObj != null) {
             album = new Album();
            try {
                JSONObject dataObj = jsonObj.getJSONObject("data");
                album.hasMore = dataObj.optString("has_more" ,"0");
                album.title = dataObj.optString("title" ,"");
                JSONArray videoArray = getJSONArray(dataObj, "type_items");
                if (videoArray != null) {
                     for (int i = 0; i < videoArray.length(); i++) {
                        JSONObject videoObj = videoArray.getJSONObject(i).getJSONObject("item");
                        album.pos = videoObj.optString("pos" ,"0");
                        }
                    }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            album.key = "0.1355";
            album.type = "index";
        }
        return album;
    }

    public static Videos parseVideo(JSONObject jsonObj ,Context context) {

        Videos videos = null;
        if (jsonObj != null) {
            videos = new Videos();
            try {
                JSONObject dataObj = jsonObj.getJSONObject("data");
                videos.videoPicUrl = getString(dataObj, "img_url");
                videos.videoPicDic = getString(dataObj, "img_dic");
                videos.videoJianjie = getString(dataObj,"shipin_jianjie");
                videos.videoApi = getString(dataObj , "api_player");

                JSONArray videoArray = getJSONArray(dataObj, "shipin_items");
                if (videoArray != null) {
                    for (int i = 0; i < videoArray.length(); i++) {
                        JSONObject videoObj = videoArray.getJSONObject(i);
                        VideoDetails videoDetails = new VideoDetails();
                        videoDetails.videoTitle = getString(videoObj, "name");
                        videoDetails.videoPicName = getString(videoObj, "pic");
                        videoDetails.videoPlayedCount = getString(videoObj, "plays");
                        videoDetails.videoYouKuId = getString(videoObj, "vid");
                        int name = Math.abs(videoDetails.videoYouKuId.hashCode());
                        if (DownloadVideoUtils.getInstance().isVideoDownloaded(name ,context)){
                            videoDetails.videoStatus = DownloadVideoService.VIDEO_DOWNED;
                        }
                        videos.videoDetailses.add(videoDetails);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return videos;
    }

    public static String parseDownloadUrl (JSONObject  object){
        String url = null;
        try {
            JSONArray jsonArray = object.getJSONArray("data");
            url = jsonArray.getString(0);
        }catch (JSONException e){
            e.printStackTrace();
        }
        return url;
    }

    public static String getString(JSONObject object, String name) {
        String content = null;
        if (object.has(name)) {
            try {
                content = object.getString(name);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return content;
    }

    public static JSONArray getJSONArray(JSONObject object, String name) {
        JSONArray jsonArray = null;
        if (object.has(name)) {
            try {
                jsonArray = object.getJSONArray(name);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return jsonArray;
    }

    public static JSONObject getJsonObjectFormBytes(byte[] responseBody){
        JSONObject result = null;
        if (responseBody == null) {
            return result;
        }
        String jsonString = TextHttpResponseHandler.getResponseString(responseBody,
                AsyncHttpResponseHandler.DEFAULT_CHARSET);
        if (jsonString != null) {
            jsonString = jsonString.trim();
            if (jsonString.startsWith(AsyncHttpResponseHandler.UTF8_BOM)) {
                jsonString = jsonString.substring(1);
            }
            if (jsonString.startsWith("{") || jsonString.startsWith("[")) {
                try {
                    Object parsed = new JSONTokener(jsonString).nextValue();
                    if (parsed instanceof JSONObject) {
                        result = (JSONObject) parsed;
                    }
                }catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }
        return result;
    }
}
