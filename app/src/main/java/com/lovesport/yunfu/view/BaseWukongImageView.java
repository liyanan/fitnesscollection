package com.lovesport.yunfu.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.lovesport.lc.AutoTextView;
import com.lovesport.lc.LC;
import com.lovesport.yunfu.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * Created by lyn on 2014/12/19.
 */
public abstract class BaseWukongImageView extends RelativeLayout {


    public Context mContext;
    public String mTitle;
    public int mTitleColor;
    public float mTitleSize;
    public String mOtherDes;
    public int mOtherDesColor;
    public float mOtherDesSize;
    public String mIndex;
    public float mIndexSize;
    public int mIndexColor;
    public float mTextContentHeight;
    public int mTextContentBg;
    public int mTextContentBgFocused;
    public int mTextContentStyle;
    public ViewGroup mTextContent;
    public AutoTextView mAlbumName;
    public AutoTextView mAlbumNum;
    public AutoTextView mAlbumIndex;
    public AutoTextView mAlbumIndexDes;
    public ImageView mAlbumPic;
    public TextView mVideoDownloaded;
    private Rect mChildRect = new Rect();
    public boolean isFocused = false;

    public BaseWukongImageView(Context context) {
        super(context);
    }

    public BaseWukongImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.BaseWukongImageView);
        //自定义的属性值
        mTitle = array.getString(R.styleable.BaseWukongImageView_titleText);
        mTitleColor = array.getColor(R.styleable.BaseWukongImageView_titleColor, Color.WHITE);
        mTitleSize = array.getDimension(R.styleable.BaseWukongImageView_titleSize, 0);
        mOtherDes = array.getString(R.styleable.BaseWukongImageView_other);
        mOtherDesColor = array.getColor(R.styleable.BaseWukongImageView_otherColor, Color.WHITE);
        mOtherDesSize = array.getDimension(R.styleable.BaseWukongImageView_otherSize, 0);
        mIndex = array.getString(R.styleable.BaseWukongImageView_index);
        mIndexColor = array.getColor(R.styleable.BaseWukongImageView_indexColor, Color.WHITE);
        mIndexSize = array.getDimension(R.styleable.BaseWukongImageView_indexSize, 0);
        mTextContentBg = array.getColor(R.styleable.BaseWukongImageView_textContentBg, 0);
        mTextContentBgFocused = array.getColor(R.styleable.BaseWukongImageView_textContentBgFocused, 0);
        mTextContentHeight = array.getDimension(R.styleable.BaseWukongImageView_textContentHeight, 0);
        mTextContentStyle = array.getInteger(R.styleable.BaseWukongImageView_textContentStyle, 0);
        array.recycle();
        initView();
        setClickable(true);
        setFocusable(true);
        setImageLoaderDefaultOption();
    }

    private void setImageLoaderDefaultOption() {
    }

    public BaseWukongImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public abstract void initView();

    public void setTextContentHeight(ViewGroup viewGroup, float height) {
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) height);
        LC.resizeLayoutParams(params);
        viewGroup.setLayoutParams(params);
    }

    public void setTextContentBgColor(int color) {
        mTextContent.setBackgroundColor(color);
    }

    @Override
    protected void onFocusChanged(boolean gainFocus, int direction, Rect previouslyFocusedRect) {
        super.onFocusChanged(gainFocus, direction, previouslyFocusedRect);
        if (gainFocus) {
            isFocused = true;
            onFocus();
        } else {
            isFocused = false;
            onLoseFocus();
        }
    }

    Paint paint = new Paint();
    int width = LC.getScaledLength(2);
    @Override
    protected boolean drawChild(Canvas canvas, View child, long drawingTime) {

        boolean returnVal =  super.drawChild(canvas, child, drawingTime);

        if (isFocused){
            paint.setColor(Color.WHITE);
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeWidth(width);
            child.getDrawingRect(mChildRect);
            offsetDescendantRectToMyCoords(child, mChildRect);
            //绘制白色边框
            canvas.drawRect(new Rect(mChildRect.left , mChildRect.top,
                    mChildRect.right , mChildRect.bottom), paint);
            canvas.save();
            canvas.restore();
        }
        return returnVal;
    }

    /**
     * 设置专辑图片
     *
     * @param imageUrl 图片链接
     */
    public void setImageView(String imageUrl, DisplayImageOptions options) {
        ImageLoader.getInstance().displayImage(imageUrl, mAlbumPic, options);
    }

    public void releaseImageViewDrawable() {
        ImageLoader.getInstance().cancelDisplayTask(mAlbumPic);
        mAlbumPic.setImageDrawable(null);
    }

    /**
     * 设置专辑名称
     *
     * @param title
     */
    public void setAlbumTitle(String title) {
        if (TextUtils.isEmpty(title)) {
            mAlbumName.setVisibility(GONE);
        } else {
            mAlbumName.setText(title);
        }
    }

    /**
     * 设置减肥指数
     *
     * @param index
     */
    public void setFitnessIndex(String index) {
        if (!TextUtils.isEmpty(index)) {
            mAlbumIndex.setText(index);
            mAlbumIndexDes.setText(mContext.getString(R.string.jianfei_zhishu));
        }
        mAlbumIndex.setVisibility(GONE);
        mAlbumIndexDes.setVisibility(GONE);
    }

    /**
     * 设置专辑详情,有多少人看过该专辑
     *
     * @param otherDes
     */
    public void setOtherDes(String otherDes) {
        if (!TextUtils.isEmpty(otherDes)) {
            mAlbumNum.setText(otherDes);
        } else {
            mAlbumNum.setVisibility(GONE);
        }
    }

    public void onFocus() {
        if (!TextUtils.isEmpty(mAlbumNum.getText())) {
            mAlbumNum.setVisibility(VISIBLE);
        } else {
            mAlbumNum.setVisibility(GONE);
        }
        setTextContentBgColor(mTextContentBgFocused);
        if (null != mAlbumIndex && null != mAlbumIndexDes && !TextUtils.isEmpty(mAlbumIndex.getText())) {
            mAlbumIndex.setVisibility(VISIBLE);
            mAlbumIndexDes.setVisibility(VISIBLE);
        }
    }

    public void onLoseFocus() {
        setTextContentBgColor(mTextContentBg);
        mAlbumNum.setVisibility(GONE);
        if (null != mAlbumIndex && null != mAlbumIndexDes) {
            mAlbumIndex.setVisibility(GONE);
            mAlbumIndexDes.setVisibility(GONE);
        }
    }

    /**
     * 视频是否下载的标记
     * @param isDownloaed
     */
    public void setVideoDownloaded(boolean isDownloaed){
        if (isDownloaed){
            mVideoDownloaded.setVisibility(VISIBLE);
        }else {
            mVideoDownloaded.setVisibility(GONE);
        }
    }

}
