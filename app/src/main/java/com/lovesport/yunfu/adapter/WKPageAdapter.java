package com.lovesport.yunfu.adapter;

import android.view.View;
import android.view.ViewGroup;

/**
 * Created by zhangge on 14-12-12.
 */
public abstract class WKPageAdapter {

//    BasePageLayout mDataObserver;

    public abstract int getCount();
    // :TODO rename it to generateView
    public abstract View getView(int position, ViewGroup parent, View convertView);
    public abstract void onBindData(int position, View contentView);
    public abstract void onUnBindData(int position, View contentView);

//    public void notifyDataSetChanged() {
//        if (mDataObserver != null) {
//            mDataObserver.notifyDataSetChanged();
//        }
//    }

//    public void setDataObserver(BasePageLayout observer) {
//        mDataObserver = observer;
//    }
}
