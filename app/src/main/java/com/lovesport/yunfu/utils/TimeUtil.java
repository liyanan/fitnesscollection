package com.lovesport.yunfu.utils;

import java.util.Formatter;
import java.util.Locale;

/**
 * Created by lyn on 2015/1/11.
 */
public class TimeUtil {


    public static String stringForTime(int timeMs) {

        StringBuilder formatbuilder = new StringBuilder();
        Formatter formatter = new Formatter(formatbuilder, Locale.getDefault());
        int totalSeconds = timeMs / 1000;

        int seconds = totalSeconds % 60;
        int minutes = (totalSeconds / 60) % 60;
        int hours   = totalSeconds / 3600;

        formatbuilder.setLength(0);
        if (hours > 0) {
            return formatter.format("%d:%02d:%02d", hours, minutes, seconds).toString();
        } else {
            return formatter.format("%02d:%02d", minutes, seconds).toString();
        }
    }

    public static double millToMin(long time){
        return ((double) time) / 60000.0f;
    }
}
