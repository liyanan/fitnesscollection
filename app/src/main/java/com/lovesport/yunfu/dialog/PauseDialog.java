package com.lovesport.yunfu.dialog;

import android.content.Context;
import android.graphics.PixelFormat;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.lovesport.yunfu.R;
import com.lovesport.yunfu.utils.Utils;
import com.umeng.analytics.MobclickAgent;

/**
 * Created by lyn on 2014/12/16.
 */
public class PauseDialog {

    private WindowManager mWindowManager;
    private Context mContext;
    private boolean isShown;
    private View mContentView;
    private PauseDialogCallBack mPauseDialogCallBack;
    private ImageView mPauseDialogImage;

    public PauseDialog(Context context) {
        mContext = context;
        initView();
    }
    private void initView(){
        mContentView = View.inflate(mContext , R.layout.dialog_pause , null);
        TextView continuePlay = (TextView) mContentView.findViewById(R.id.dialog_pause_btn);
        mPauseDialogImage = (ImageView)mContentView.findViewById(R.id.dialog_pause_image);
        Utils.setImageDrawable(mPauseDialogImage, mContext, R.drawable.dialog_pause_girl);
        continuePlay.requestFocus();
        //确定键
        continuePlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MobclickAgent.onEvent(mContext , "pause_dialog_continue_play");
                dismiss();
                if (mPauseDialogCallBack != null){
                    mPauseDialogCallBack.continuePlay();
                }
            }
        });
        //返回键
        continuePlay.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN){
                    MobclickAgent.onEvent(mContext , "pause_dialog_back");
                    dismiss();
                    if (mPauseDialogCallBack != null){
                        mPauseDialogCallBack.continuePlay();
                    }
                    return true;
                }
                return false;
            }
        });

    }

    public interface PauseDialogCallBack{
        public void continuePlay();
    }

    public void setCallBack(PauseDialogCallBack callBack){
        this.mPauseDialogCallBack = callBack;
    }

    public void show() {
        mWindowManager = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        layoutParams.flags = WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM ;
        layoutParams.type=WindowManager.LayoutParams.TYPE_SYSTEM_ALERT;
        layoutParams.format = PixelFormat.TRANSLUCENT;
        layoutParams.windowAnimations = R.style.anim_view;
        layoutParams.x = 0;
        layoutParams.y = 0;
        if (!isShown){
            try {
                mWindowManager.addView(mContentView , layoutParams);
                isShown = true;
                MobclickAgent.onEvent(mContext , "pause_dialog_show");
            }catch (Exception e){
                isShown = false;
            }
        }
    }

    public void dismiss() {
        if (mContentView != null && isShown){
            try{
                mWindowManager.removeView(mContentView);
                isShown = false;
                Utils.removeImage(mPauseDialogImage);
            }catch (Exception e){
            }
        }
    }
}
